import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Linking, Dimensions, Platform, PixelRatio, Image, LogBox } from 'react-native';
import * as Font from "expo-font";
import { StatusBar } from 'expo-status-bar';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { FontAwesome, FontAwesome5 } from '@expo/vector-icons';

import { DottedLine, commonStyle, itemColor, HeaderLogoImage, AmyStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, white, darkBlue } from './style/CommonStyle';
import { MyAccountPage, EmptyPage, RescheduleNextPayment, NextPayment, MonthlySummary, MakeAPaymentStep1,
MakeAPaymentStep2, MakeAPaymentStep3, CreditSummary, PaymentHistory, PaymentPlan, Dos, Donts, Support,
WhatAffectsYourCreditScore, ViewScheduledPayments, ScheduledPaymentTile, BlogThumbnail, CurvedTile,
PaymentProgress, CurrentPayment, ProgressReport, EducationCenter, LoginPageIos, LoginPageAndroid, NewPassword } from './Components/index';

const Drawer = createDrawerNavigator();

//TODO: Remove 2nd line after finishing all navigation
const pagesInMenu = ['Support', 'MyAccount', 'Logout',
                    '            ', 'PaymentPlan', 'EducationCenter', 'ChangePassword',
                    'RescheduleNextPayment', 'NextPayment', 'MonthlySummary', 'MakeAPayment-StepOne',
                    'MakeAPayment-StepTwo', 'MakeAPayment-StepThree', 'ViewScheduledPayments',
                    'CreditSummary', 'PaymentHistory', 'ProgressReport', 'NewPassword', 'sadieHomePage'];

LogBox.ignoreAllLogs();

class App extends React.Component {

    state = {
        appIsReady: false
    }

    async componentDidMount() {
        this.prepareResources();
    }

    prepareResources = async () => {
        await cacheAssets();
        this.setState({appIsReady: true});
    }

    render() {
        if (!this.state.appIsReady) {
            return <Text style={{top: 100, left: 50}}>Loading...</Text>
        }
        else {
            return (
                <NavigationContainer>
                    <Drawer.Navigator initialRouteName='PaymentPlan' drawerPosition='right'
                        overlayColor="transparent"
                        screenOptions={{ headerShown: false }}
                        style={{backgroundColor: 'white'}}
                        drawerStyle={{width: 210, padding: 0, }} //height: 300
                        drawerContent={(props) => {
                            const filteredProps = {
                                ...props,
                                state: {
                                ...props.state,
                                routeNames: props.state.routeNames.filter(
                                    (routeName) => {
                                    pagesInMenu.includes(routeName)
                                    },
                                ),
                                routes: props.state.routes.filter(
                                    (route) =>
                                    pagesInMenu.includes(route.name)
                                    ),
                                },
                            };
                              return (
                                <DrawerContentScrollView {...filteredProps} contentContainerStyle={{ paddingLeft: 0}}>
                                  <DrawerItem label="Close" labelStyle={commonStyle.navigationItems} icon={() => <FontAwesome name="times" style={[commonStyle.icons,commonStyle.navIcons]}/>} onPress={() => props.navigation.closeDrawer()}/>
                                  <DrawerItemList {...filteredProps} labelStyle={commonStyle.navigationItems} />
                                </DrawerContentScrollView>
                              );
                            }}>
                        <Drawer.Screen name='Support' options={{drawerLabel: 'Support', drawerIcon: () => <FontAwesome name="question-circle-o" style={[commonStyle.icons, commonStyle.navIcons]}/>}} component={Support} />
                        <Drawer.Screen name='MyAccount' options={{drawerLabel: 'My Account', drawerIcon: () => <FontAwesome name="user-circle" style={[commonStyle.icons, commonStyle.navIcons]}/>}} component={MyAccountPage} />
                        <Drawer.Screen name='Logout' options={{drawerLabel: 'Logout', drawerIcon: () => <FontAwesome5 name="sign-out-alt" style={[commonStyle.icons, commonStyle.navIcons]}/>}} component={ios ? LoginPageIos : LoginPageAndroid}/>

                        <Drawer.Screen name='            ' component={EmptyPage}/>
                        <Drawer.Screen name='EmptyPage' options={{drawerLabel: '            '}} component={EmptyPage}/>

                        <Drawer.Screen name='PaymentPlan' options={{drawerLabel: 'Payment Plan'}} component={PaymentPlan}/>
                        <Drawer.Screen name='EducationCenter' options={{drawerLabel: 'Education Center'}} component={EducationCenter} />
                        <Drawer.Screen name='ChangePassword' options={{drawerLabel: 'Change Password'}} component={NewPassword} />
                        <Drawer.Screen name='RescheduleNextPayment' options={{drawerLabel: 'Reschedule Next Payment'}} component={RescheduleNextPayment} />
                        <Drawer.Screen name='NextPayment' options={{drawerLabel: 'Next Payment'}} component={NextPayment} />
                        <Drawer.Screen name='MonthlySummary' options={{drawerLabel: 'Monthly Summary'}} component={MonthlySummary} />
                        <Drawer.Screen name='MakeAPayment-StepOne' options={{drawerLabel: 'Make A Payment'}} component={MakeAPaymentStep1} />
                        <Drawer.Screen name='MakeAPayment-StepTwo' options={{drawerLabel: 'Make A Payment - Step Two'}} component={MakeAPaymentStep2} />
                        <Drawer.Screen name='MakeAPayment-StepThree' options={{drawerLabel: 'Make A Payment - Step Three'}} component={MakeAPaymentStep3} />
                        <Drawer.Screen name='ViewScheduledPayments' options={{drawerLabel: 'View Scheduled Payments'}} component={ViewScheduledPayments} />
                        <Drawer.Screen name='CreditSummary' options={{drawerLabel: 'Credit Summary'}} component={CreditSummary} />
                        <Drawer.Screen name='PaymentHistory' options={{drawerLabel: 'Payment History'}} component={PaymentHistory} />
                        <Drawer.Screen name='ProgressReport' options={{drawerLabel: 'Progress Report'}} component={ProgressReport} />
                        <Drawer.Screen name='NewPassword' options={{drawerLabel: 'New Password'}}component={NewPassword} />
                    </Drawer.Navigator>
                </NavigationContainer>
            );
        }
    }
}

async function cacheAssets() {
  const fontAssets = cacheFonts([
    { medium: require("./assets/fonts/ProximaNovaRegular.otf") },
    { bold: require("./assets/fonts/ProximaNovaBold.otf") },
    { condensed: require("./assets/fonts/ProximaNovaCondensedSemibold.otf") }
  ]);
  await Promise.all([...fontAssets]);
}

function cacheFonts(fonts) {
  return fonts.map((font) => Font.loadAsync(font));
}

cacheAssets();

const paymentProgressInfo = {
  "progress": 0.1,
  "date": "3/2/2025",
  "total": "25,328.03"
};

export default App;
