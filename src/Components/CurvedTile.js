import React from 'react';
import { Text, View, Dimensions, PixelRatio, StyleSheet } from 'react-native';
import curvedTileStyle from '../style/CurvedTileStyle';

const CurvedTile = (props) => {
  return (
    <View style={curvedTileStyle.container}>
        <View style={curvedTileStyle.leftBase}>
            <View style={curvedTileStyle.leftCircle}>
                <Text style={curvedTileStyle.percentText}>
                    {props.percent}%
                </Text> 
            </View>
            <Text style={curvedTileStyle.titleText}>{props.title}</Text> 
        </View>
        <View style={curvedTileStyle.rightBase}>
            <Text style={curvedTileStyle.descriptionText}>
                {props.description}
            </Text>
            <Text style={curvedTileStyle.impactText}>
                * HIGH IMPACT
            </Text>
        </View>
    </View>
  );
};

export default CurvedTile;
