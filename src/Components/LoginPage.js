import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, Image, ScrollView, TextInput, Button, StyleSheet } from 'react-native';
import image from 'react-native'

const LoginPage = (props) => {
    return (
        <View>
            <Image source={require('../assets/images/TCABanner.png')}
                            style={{ marginTop: 10, marginBottom: 50, width: 400, height: 70 }} />
            <View style={{flex: .2, backgroundColor: "rgba(1,74,127,255)"}}>
                <Text style={styles.centerTextWhite}>COVID-19 UPDATE</Text>
            </View>
            <View style={styles.container}>
                <Text style={styles.standardText}>          Email<Text style={styles.redText}>*</Text></Text>
                <TextInput
                    style={styles.input}
                />
                <Text>{"\n"}</Text>
                <Text style={styles.standardText}>          Password<Text style={styles.redText}>*</Text></Text>
                <TextInput
                    style={styles.input}
                />
            </View>
            <View style={styles.loginButtonSection}>
                <Button
                    color="rgba(1,74,127,255)"
                    title="Log In "
                />
            </View>
            <View>
                <Text style={styles.centerTextBold}>Register Here >> </Text>
                <Text>{"\n"}</Text>
                <Text style={styles.centerTextBold}>Having trouble signing in?</Text>
                <Text style={styles.centerText}>Call a counselor at</Text>
                <Text style={styles.centerText}>800-823-7396</Text>
                <Text style={styles.centerText}>Mon - Fri 6am - 6pm</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: .7,
        justifyContent: 'center',
        marginHorizontal: 10,
    },
    loginButton: {
        color: "rgba(1,74,127,255)"
    },
    loginButtonSection: {
        width: '100%',
        height: '10%',
        alignSelf: 'center'
    },
    input: {
        margin: 1,
        height: 40,
        width: 300,
        borderColor: 'rgba(1,74,127,255)',
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center'
    },
    standardText: {
        color: 'rgba(1,74,127,255)'
    },
    centerText: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'rgba(1,74,127,255)'
    },
    centerTextWhite: {
        fontSize: 25,
        fontWeight: "bold",
        textAlign: 'center',
        color: 'white'
    },
    centerTextBold: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: "bold",
        color: 'rgba(1,74,127,255)'
    },
    redText: {
        color: "red"
    }

})

export default LoginPage;