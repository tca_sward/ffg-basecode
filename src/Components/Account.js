import React from 'react';
import { Text, View, ScrollView} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { Tooltip } from 'react-native-elements';
import currentPaymentStyle from '../style/CurrentPaymentStyle.js';
import { commonStyle, itemColor, moderateScale, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue} from  '../style/CommonStyle';

import * as Progress from 'react-native-progress';
import paymentProgressStyle from '../style/PaymentProgressStyle.js';

const Account = (props) => {
    return (
        <View>
            <ScrollView>
                <View style={currentPaymentStyle.imageStyle}>
                    <Text style={currentPaymentStyle.whiteTextStyle}>
                        Active
                    </Text>
                </View>
                <View style={commonStyle.roundedRectangleBottomStyle}>
                    <View>
                        <Text style={commonStyle.paragraphText}>{props.bank_name}</Text>
                    </View>
                    <View style={{width: "90%", marginBottom: '5%'}}>
                        <Progress.Bar style={paymentProgressStyle.progressBarStyle}
                                        progress={ 1 - (props.remaining_balance/props.original_balance) }
                                        color={itemColor}
                                        height={100}
                                        borderColor={'white'}
                                        width={null}>
                            <Text style={{alignSelf:"center",
                                            color:"white",
                                            position:"absolute",
                                            top:'25%',
                                            fontFamily:'bold',
                                            fontSize: moderateScale(15)}}>
                                ${props.original_balance - props.remaining_balance}
                            </Text>
                        </Progress.Bar>
                    </View>
                    <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                        <Text style={commonStyle.paragraphText}>${props.remaining_balance} Remaining</Text>
                        <Tooltip withOverlay={false} backgroundColor={'white'} width={180} height={100} containerStyle={{bottom: 180, left: 220, padding: 5, borderRadius: 0, borderWidth: '.3px', borderColor: '#A8A8A8'}} popover={<Text style={{color: '#777778', fontSize: font13}}>This is an estimate. Please refer to your monthly creditor statements for the most up-to-date balance information.</Text>}>
                            <FontAwesome name='question-circle' style={[commonStyle.fontAwesomeStyle, {marginTop: 5, marginLeft: 5}]}/>
                        </Tooltip>
                    </View>
                    <View>
                        <Text style={commonStyle.paragraphText}>${props.original_balance} Original Balance</Text>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

export default Account;
