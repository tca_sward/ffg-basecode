import React, {Component, useState} from 'react';
import { SafeAreaView, StyleSheet, Text, TextInput, View, Button, TouchableOpacity, Image, ScrollView, LogBox } from 'react-native';
import { Switch } from 'react-native-switch';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import CurrencyInput from 'react-native-currency-input';
import Moment from 'moment';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class MakeAPaymentStep1 extends Component {

    constructor(props: Props) {
        super(props);
        this.state = {
            paymentTypeSelected: this.props.makeAPaymentValues.paymentTypeSelected,
            paymentAmountMonthly: this.props.makeAPaymentValues.paymentAmountMonthly,
            paymentAmountSelected: this.props.makeAPaymentValues.paymentAmountSelected,
            paymentDateSelected: this.props.makeAPaymentValues.paymentDateSelected,
            paymentDateMinimum: this.props.makeAPaymentValues.paymentDateMinimum,
            paymentDateMaximum: this.props.makeAPaymentValues.paymentDateMaximum,
            paymentDateChange: this.props.makeAPaymentValues.paymentDateChange,
            accountSelected: this.props.makeAPaymentValues.accountSelected,
            accountSelectedMonthly: this.props.makeAPaymentValues.accountSelectedMonthly,
            accounts: this.props.makeAPaymentValues.accounts,
            showCalendar: this.props.makeAPaymentValues.showCalendar,
            paymentTypeDropdownOpen: this.props.makeAPaymentValues.paymentTypeDropdownOpen,
            accountSelectedDropdownOpen: this.props.makeAPaymentValues.accountSelectedDropdownOpen,
            refreshed: false
        };

        this.defaultState = JSON.parse(JSON.stringify(this.state));
        this.setPaymentTypeSelected = this.setPaymentTypeSelected.bind(this);
        this.setAccountSelected = this.setAccountSelected.bind(this);

    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps != null && nextProps.refresh != null) {
            if (nextProps.refresh === true && state.refreshed === false) {
                return {paymentTypeSelected: nextProps.makeAPaymentValues.paymentTypeSelected,
                        paymentAmountMonthly: nextProps.makeAPaymentValues.paymentAmountMonthly,
                        paymentAmountSelected: nextProps.makeAPaymentValues.paymentAmountSelected,
                        paymentDateSelected: nextProps.makeAPaymentValues.paymentDateSelected,
                        paymentDateMinimum: nextProps.makeAPaymentValues.paymentDateMinimum,
                        paymentDateMaximum: nextProps.makeAPaymentValues.paymentDateMaximum,
                        paymentDateChange: nextProps.makeAPaymentValues.paymentDateChange,
                        accountSelected: nextProps.makeAPaymentValues.accountSelected,
                        accountSelectedMonthly: nextProps.makeAPaymentValues.accountSelectedMonthly,
                        accounts: nextProps.makeAPaymentValues.accounts,
                        showCalendar: nextProps.makeAPaymentValues.showCalendar,
                        paymentTypeDropdownOpen: nextProps.makeAPaymentValues.paymentTypeDropdownOpen,
                        accountSelectedDropdownOpen: nextProps.makeAPaymentValues.accountSelectedDropdownOpen,
                        refreshed: true
                }
            }
        }
    }

    setPaymentTypeSelected(callback) {
        this.setState(state => ({
            paymentTypeSelected: callback(state.value)
        }));
    }

    setAccountSelected(callback) {
        this.setState(state => ({
            accountSelected: callback(state.value)
        }));
    }

    refreshStepOne = (refreshBoolean) => {
        if (refreshBoolean)
            this.setState(this.defaultState)
        }

    render() {

        const paymentTypes = ['Monthly', 'Extra', 'Partial']
        const paymentTypesList = paymentTypes.map((type) => ({label:type, value:type}));
        const accountsList = this.state.accounts.map((type) => ({label:type, value:type}));
        const monthly = this.state.paymentTypeSelected === 'Monthly'
        const noType = this.state.paymentTypeSelected === null
        const noAccountSelected = this.state.accountSelected === ''
        const noAccountPreference = this.state.accountSelected === 'No Preference'
        const today = new Date()


        var bankAccount = () => {
            if (monthly || noType || noAccountSelected || this.state.accountSelected === 'No Preference'){
                return (this.state.accountSelectedMonthly.substring(this.state.accountSelectedMonthly.length - 4));
            } else {
                return (this.state.accountSelected.substring(this.state.accountSelected.length - 4));
            }
        };

        function App() {
        const [open, setOpen] = useState(false);
                const [value, setValue] = useState(null);
                const [items, setItems] = useState([
                    {label: 'Apple', value: 'apple'},
                    {label: 'Banana', value: 'banana'}
                ]);
        }



        const checkSubmitValues = () => {
            if (!paymentTypes.includes(this.state.paymentTypeSelected)) {
                alert("Please choose the type of payment")
            } else {
                if (this.state.paymentTypeSelected !== 'Monthly' && (parseFloat(this.state.paymentAmountSelected) > 25000.00 || parseFloat(this.state.paymentAmountSelected) <= 0.00 )) {
                    alert("Please enter a payment amount")
                } else {
                    if (this.state.paymentDateChange == false) {
                        alert("Please select a date to schedule the payment")
                    } else {
                          this.props.navigation.navigate('MakeAPayment-StepTwo', {
                              paymentTypeSelected: this.state.paymentTypeSelected,
                              paymentAmountSelected: monthly ? this.state.paymentAmountMonthly:this.state.paymentAmountSelected,
                              paymentDateSelected: formatDate(this.state.paymentDateSelected),
                              accountSelected: (monthly||noAccountSelected||noAccountPreference) ? this.state.accountSelectedMonthly:this.state.accountSelected,
                              refreshStepOne: this.refreshStepOne
                          })
                      }
                  }
            }

        };


        return (
            <View style={commonStyle.body}>
                <Text style={styles.subTitle}>Step 1: Select Payment Details</Text>
                <Image source={require('../assets/images/step1Bar.png')} style={commonStyle.stepBar} />
                <View style={{flexDirection: 'column', marginTop: 20, marginLeft: marginLeftBody}}>
                    <Text style={styles.label}>Payment Type</Text>
                    <View style={[styles.pickerContainer, ios? {zIndex: 10}:null]}>
                        <DropDownPicker
                        open={this.state.paymentTypeDropdownOpen}
                        items={paymentTypesList}
                        value={this.state.paymentTypeSelected}
                        style={{borderWidth: 0}}
                        textStyle={{justifyContent: 'flex-start'}}
                        labelStyle={{justifyContent: 'flex-start', marginLeft: 0}}
                        setValue={this.setPaymentTypeSelected}
                        onPress={open=> this.setState({ paymentTypeDropdownOpen:!this.state.paymentTypeDropdownOpen })}
                        onChangeValue={value => this.setState({ paymentTypeDropdownOpen:!this.state.paymentTypeDropdownOpen })}
                        />
                    </View>
                    <Text style={styles.label}>Payment Amount</Text>
                    <View style={[styles.value, {marginTop: ios ? 10:0, width: 70}]}>
                        { monthly || noType ?
                            <CurrencyInput style={{padding: 0, color: 'black'}} editable={false} unit="$" delimiter="," separator="." precision={2} value={this.state.paymentAmountMonthly}/> :
                            <CurrencyInput style={{padding: 0, color: 'black'}} editable={true} unit="$" delimiter="," separator="." precision={2} maxValue={25000} keyboardType='numeric' onChangeValue={value => this.setState({ paymentAmountSelected:value })} value={this.state.paymentAmountSelected}/>
                        }
                    </View>
                    <Text style={styles.label}>Payment Date</Text>
                    <TouchableOpacity onPress={() => { this.setState({showCalendar: !this.state.showCalendar, paymentDateChange: true}) }}>
                         <Text style={styles.value}>{this.state.paymentDateChange ? formatDate(this.state.paymentDateSelected):'MM/DD/YYYY'}</Text>
                    </TouchableOpacity>
                    {this.state.showCalendar &&
                        <DateTimePicker
                          testID="dateTimePicker"
                          display={ios ? "spinner":"calendar"}
                          value={this.state.paymentDateSelected}
                          minimumDate={this.state.paymentDateMinimum}
                          maximumDate={this.state.paymentDateMaximum}
                          mode="date"
                          format="MM/DD/YYYY"
                          onChange={(event, date) =>{this.setState({showCalendar: ios ? this.state.showCalendar:!this.state.showCalendar, paymentDateChange: true, paymentDateSelected: date})}}
                        />
                    }
                    <Text style={styles.label}>Bank Account</Text>
                    <Text style={styles.value}>#{bankAccount()}</Text>

                    { !monthly && !noType &&
                        <View>
                            <Text style={styles.label}>Account</Text>
                            <View style={[styles.pickerContainer, ios? {zIndex: 10}:null, {marginBottom: 80, width: 200}]}>
                                <DropDownPicker
                                open={this.state.accountSelectedDropdownOpen}
                                items={accountsList}
                                value={this.state.accountSelected}
                                style={{borderWidth: 0}}
                                textStyle={{justifyContent: 'flex-start'}}
                                labelStyle={{justifyContent: 'flex-start',  marginLeft: 0}}
                                setValue={this.setAccountSelected}
                                onPress={open=>this.setState({ accountSelectedDropdownOpen:!this.state.accountSelectedDropdownOpen })}
                                onChangeValue={value=>this.setState({ accountSelectedDropdownOpen:!this.state.accountSelectedDropdownOpen })}
                                />
                            </View>
                        </View>
                    }
                </View>
                <TouchableOpacity style={{alignSelf: 'flex-end'}} onPress={() => { this.setState(this.defaultState), this.props.makeAPaymentCollapseCallback(true) }}>
                    <Text style={[styles.value, {textDecorationLine: 'underline'}]}>
                        Cancel
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity title="Continue" style={commonStyle.blueButtonTouchable} onPress={() => { checkSubmitValues() }}>
                    <Text style={commonStyle.blueButtonText}>Continue</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    label: {
        marginTop: 15,
        fontSize: font15,
        fontWeight: 'bold',
        color: darkBlue,
        textTransform: 'uppercase',
    },
    pickerContainer: {
        minHeight: 30,
        width: 120,
        marginLeft: -10,
    },
    subTitle: {
        marginTop: 20,
        fontSize: font20,
        color: '#4dd2f0',
        fontWeight: 'bold',
    },
    value: {
        marginTop: 10,
        fontSize: font15,
    },
});

const formatDate = (date) => {
  return (
    Moment(date).format('MM/DD/YYYY')
  );
};

