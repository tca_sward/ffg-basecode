/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow strict-local
 * @format
 */

import type {Node} from 'react';
import {Platform, StyleSheet, Text} from 'react-native';
import React from 'react';
import { commonStyle, darkBlueText, moderateScale } from '../style/CommonStyle.js';

const styles = StyleSheet.create({
  highlight: {
    fontFamily: 'bold',
    color: darkBlueText,
  },
});

const Donts = (): Node => (
    <Text>
      <Text style={styles.highlight}>Don't</Text> Miss payment due dates {'\n\n'}
      <Text style={styles.highlight}>Don't</Text> Make only minimum payments {'\n\n'}
      <Text style={styles.highlight}>Don't</Text> Keep credit card balances at or near their limit
      {'\n\n'}
      <Text style={styles.highlight}>Don't</Text> Apply for new credit; work on reducing your debt
      instead {'\n\n'}
      <Text style={styles.highlight}>Don't</Text> Close old credit accounts; it can ding
      your score
    </Text>

  );

export default Donts;
