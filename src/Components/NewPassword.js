import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaView, View, Text, Image, ScrollView, TextInput, Button, StyleSheet, Platform, Linking } from 'react-native';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import CommonStyle, { ios } from '../style/CommonStyle';

const NewPassword = (props) => {
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
            <HeaderComponent navigation={props.navigation}/>
                <View style={styles.container}>
                    <Text style={styles.standardText}>Please create a new{"\n"}password.</Text>
                    <Text style={styles.blueText}>Old Password<Text style={styles.redText}>*</Text></Text>
                    <TextInput style={styles.input}/>
                    <Text style={styles.blueText}>New Password<Text style={styles.redText}>*</Text></Text>
                    <TextInput style={styles.input}/>
                    <Text style={styles.blueText}>Confirm New Password<Text style={styles.redText}>*</Text></Text>
                    <TextInput style={styles.input}/>
                    <Text style={styles.standardText}>Cancel</Text>
                </View>
                <View style={styles.loginButtonSection}>
                    <Button
                        title="Save"
                        raised={true}
                        color='white'
                    />
                </View>
            <FooterComponent navigation={props.navigation}/>
        </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    container: {
        // flex: .7,
        marginTop: '10%',
        marginBottom: '5%',
        marginLeft: '12%',
        justifyContent: 'center',
        // marginHorizontal: 10,
    },
    containerBlack: {
        backgroundColor: 'black',
        height: '25%',
        width: '100%',
        color: "white"
    },
    loginButton: {
        color: "rgba(1,74,127,255)"
    },
    loginButtonSection: {
        width: '20%',
        // alignSelf: 'center',
        backgroundColor:'rgba(1,74,127,255)',
        borderRadius: 8,
        marginTop: '0%',
        marginBottom: '10%',
        marginLeft: '12%'
    },
    input: {
        margin: 1,
        height: 40,
        width: 300,
        borderColor: 'rgba(1,74,127,255)',
        borderWidth: 1,
        borderRadius: 5,
    },
    standardText: {
        marginTop: 20,
        color: 'black',
        fontSize: 18
    },
    blueText: {
        color: 'rgba(1,74,127,255)',
        fontSize: 10,
        marginTop: 20
    },
    centerText: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'rgba(1,74,127,255)',
    },
    underlineCenterText: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'rgba(1,74,127,255)',
        textDecorationLine: 'underline'
    },
    centerTextSmall: {
        fontSize: 10,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white'
    },
    centerTextWhite: {
        fontSize: 25,
        fontWeight: "bold",
        textAlign: 'center',
        color: 'white'
    },
    centerTextBold: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: "bold",
        color: 'rgba(1,74,127,255)',
        fontSize: 14
    },
    redText: {
        color: "red"
    },
    smallText: {
        fontSize: 10,
        color: 'rgba(1,74,127,255)'
    }

})

export default NewPassword;