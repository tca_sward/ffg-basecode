import React, { useState, useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Linking, SafeAreaView} from 'react-native';
import BlogThumbnail from './BlogThumbnail.js';
import CurvedTile from './CurvedTile';
import { Button } from 'react-native-elements/dist/buttons/Button';
import { commonStyle, moderateScale, verticalScale, AmyStyle, DottedLine } from '../style/CommonStyle.js';
import currentPaymentStyle from '../style/CurrentPaymentStyle.js';
import DropDownPicker from 'react-native-dropdown-picker';
import HeaderComponent from './HeaderComponent.js';
import FooterComponent from './FooterComponent.js';
import { max } from 'moment';

const EducationCenter = (props) => {
  
  const curvedTileInfo = [
    {
      "title": "PAYMENT HISTORY",
      "percent": 35,
      "description": "Includes on-time payments, collection accounts, charge offs, tax liens, forclosures, etc"
    }
  ]

  var [currentBlogPosts, setCurrentBlogPosts] = useState([])
  var [maxPosts, setMaxPosts] = useState([])
  var [allItems, setAllItems] = useState([])
  var [isLoaded, setIsLoaded] = useState(false)
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    {"label": "All", "value": "0"},
    {"label": "Credit and Debit", "value": "79"},
    {"label": "Student Loans", "value": "80"},
    {"label": "Housing", "value": "81"},
    {"label": "Budgeting", "value": "82"},
    {"label": "Saving", "value": "83"},
    {"label": "Fraud Awareness", "value": "84"}
  ]);

  useEffect(() => {
    if (value == "0" || value == null) {
      setCurrentBlogPosts(maxPosts)
    } else {
      var article_type = parseInt(value)
      var tempList = []
      
      maxPosts.map(element => {
        element["article_type"].forEach((eType) => {
          if (article_type == eType) {
            tempList.push(element)
          }
        })
        if (article_type in element["article_type"] ) {
            tempList.push(element)
        }
      })
      setCurrentBlogPosts(tempList)
    }
  },[value, maxPosts])

  useEffect(() => {
    var url = "https://takechargeamerica.org/wp-json/wp/v2/posts?per_page=3"
    fetch(url, {headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }})
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setAllItems(result);
        },
        (error) => {
          setIsLoaded(true);
        }
      )
  }, [])

  useEffect(() => {
    var tempArray = []

    allItems.map((item, index) => {
      tempArray.push({
        "title": item["title"]["rendered"],
        "imageUri": item["jetpack_featured_media_url"],
        "link": item["link"],
        "article_type": item["categories"],
        "key": index
      })
    })

    setMaxPosts(tempArray)

  }, [allItems])

  const addMoreBlogPosts = () => {   
    var numberOfPosts = maxPosts.length + 3
    
    var url = "https://takechargeamerica.org/wp-json/wp/v2/posts?per_page=" + numberOfPosts
    fetch(url, {headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }})
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setAllItems(result);
        },
        (error) => {
          setIsLoaded(true);
        }
      )
  }

  const BlogPostDisplays = () => {
    
    if (currentBlogPosts.length < 1) {
      return (<Text style={commonStyle.paragraphText}> No results. </Text>);
    } else if (maxPosts.length > 8) {
      return (
        currentBlogPosts.map((element) => 
          <BlogThumbnail image={element["imageUri"]} title={element["title"]} percent={element["percent"]} link={element["link"]}></BlogThumbnail>
        )
      );
    } else {
      return (
        <View>
            {
              currentBlogPosts.map((element) => 
                <BlogThumbnail image={element["imageUri"]} title={element["title"]} percent={element["percent"]} link={element["link"]}></BlogThumbnail>
              )
            }   
            <View style={{padding: '5%'}}></View>
            <TouchableOpacity onPress={addMoreBlogPosts} style={commonStyle.blueButtonTouchable}>
            <Text style={commonStyle.footerText}>
              Load More From the Blog
            </Text>
            </TouchableOpacity>
            <View style={{padding: '10%'}}></View>
        </View>
      );
    }
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <HeaderComponent navigation={props.navigation}/>
      <ScrollView style={{marginLeft: 0}} contentContainerStyle={{paddingBottom: 0}}>
          <View style={{margin: '5%'}}>
          <Text style={[commonStyle.pageHeader, {fontSize: moderateScale(40, 0.5)}]}>Education Center</Text>
          <DottedLine/>
            <Text style={[commonStyle.paragraphText, {marginTop: 20}]}>
                        We say it all the time because it's true: knowledge is power.
                        Here's where you'll find plenty of helpful information on saving money, living a cash-only lifestyle,
                        planning your financial future and much more. We add new articles every week, so be sure to check back often.
            </Text>
            <TouchableOpacity onPress={() => props.navigation.navigate('Support')}>
                    <Text style={styles.pageLink}>
                        Frequently Asked Questions >>
                    </Text></TouchableOpacity>
            <TouchableOpacity onPress={() => Linking.openURL('http://takechargeamerica.org/reviews')}>
            <Text style={styles.pageLink} >
                TCA Success Stories >>
            </Text></TouchableOpacity>
            <TouchableOpacity onPress={() => Linking.openURL
                      ('http://takechargeamerica.org/financial-education/finance-calculator')}>
                    <Text style={styles.pageLink}>
                        Financial Calculators >>
                    </Text></TouchableOpacity>
          </View>
          <Text style={[commonStyle.sectionHeader, {marginTop: 40, marginBottom: 20, marginLeft: 30}]}>From the Blog</Text>
          <View style={{
                borderStyle: 'dotted',
                borderColor: 'lightgrey',
                borderWidth: 1,
                borderRadius: 1,
                marginBottom: "5%"
              }}>
            </View>
          <View style={{padding: '5%', paddingBottom: '15%', minHeight: 500}}>
            <Text style={commonStyle.fieldLabel}> Sort By</Text>
            <View style={{zIndex: 200, bottomMargin: '10%'}}>
              <DropDownPicker
                style={commonStyle.pickerStyle}
                open={open}
                value={value}
                items={items}
                setOpen={setOpen}
                setValue={setValue}
                setItems={setItems}
              />
            </View>
            <BlogPostDisplays></BlogPostDisplays>
          </View>                 
        </ScrollView>
        <FooterComponent navigation={props.navigation}/>
      </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionDescription: {
    marginTop: 12,
    marginBottom: 12,
    fontSize: moderateScale(18),
    fontFamily: 'medium',
    color: 'black'
  },
  sectionHeaderPadding: {
    marginBottom: 12,
    marginTop: 12,
  },
    companyInfo: {
      fontSize: moderateScale(24),
      fontFamily: 'medium',
      alignItems: 'center',
      justifyContent: 'center',
      color: 'black'
    },
   companyURL: {
     fontSize: moderateScale(24),
     fontFamily: 'medium',
     color: 'blue',
     alignItems: 'center',
     justifyContent: 'center',
   },
    imageStyle: {
        width: 150,
        height: 50,
        flex: 1,
        resizeMode: 'contain'
    },
    pageLink:{
        fontFamily:'medium',
        textTransform: 'uppercase',
        marginTop: 20,
        textAlign: 'left',
        color: 'blue',
        fontWeight: 'bold'
    },
    thumbnail: {
          alignItems: 'center',
          flex: 1
        },

});

export default EducationCenter;