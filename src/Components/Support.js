import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  View,
  Text,
  StatusBar,
  Divider,
  Image,
  Linking,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { List } from 'react-native-paper';
import { commonStyle, DottedLine } from '../style/CommonStyle.js';
import accordionStyle from '../style/AccordionStyle.js';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';

//TODO: The phone numbers need to be made interactive as well
const numTitleLines = 3;
const numDescriptionLines = 18;

export default class Support extends Component {

  constructor(props: Props) {
      super(props);
      this.state = {

      }
  }
  render() {
  return (
  <>
   <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
   <HeaderComponent navigation={this.props.navigation}/>
   <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={{margin: 10, top: 10, paddingBottom: 100, bottom: 110}}>

      <Text style={accordionStyle.pageTitle}>Support</Text>
      <DottedLine></DottedLine>

     <Text style={accordionStyle.sectionHeader}>Frequently Asked Questions</Text>

    <List.Section>
      <List.Accordion
        title="What happens if I miss a payment?"
        titleStyle={accordionStyle.sectionTitle}
        titleNumberOfLines={numTitleLines}>
        <Text style={accordionStyle.sectionDescription}>It's important to make your DMP payments as scheduled so your creditors
          receive payments on time. If a creditor does not receive a full payment in a
          30-day billing cycle, they may discontinue benefits. Additionally, past-due
          accounts may negatively affect your credit history and/or your creditors may
          charge off the accounts. Please call a counselor right away if you can't make
          your payment. We will review your options to help you stay on track.</Text>
      </List.Accordion>

      <List.Accordion
              title="Can I make extra payments?"
              titleStyle={accordionStyle.sectionTitle}>
            <Text style={accordionStyle.sectionDescription}>
            Yes. Please make any extra payments through Take Charge America, rather than
                        paying a creditor directly, which may jeopardize your program benefits. When making an
                        extra payment through Take Charge America, we recommend you:
                                   {'\n\t'} o Address delinquencies first, if applicable.
                                   {'\n\t'} o Apply the extra funds to the highest interest account or a low balance
                                    account you can pay off quickly.</Text>
            </List.Accordion>
    <List.Accordion
                 title="How do I change my EFT draft date?"
                 titleStyle={accordionStyle.sectionTitle}
                 titleNumberOfLines={numTitleLines}>
                <Text style={accordionStyle.sectionDescription}>
                Please call a counselor during regular business hours to change
                                  your scheduled draft date.
                </Text>
               </List.Accordion>
    <List.Accordion
                 title="Can I open new lines of credit while enrolled in a Debt Management Plan?"
                 titleStyle={accordionStyle.sectionTitle}
                 titleNumberOfLines={numTitleLines}>
                <Text style={accordionStyle.sectionDescription}>
                We recommend that clients enrolled in a DMP do not open new lines of credit until the current debt is paid off.
                Creditors may discontinue your plan benefits if you open new credit.
                Vehicle and housing loans are unique and may be necessary while you are enrolled in a DMP.
                We will provide lenders with your TCA payment history to assist with such loans upon your request.
                </Text>
               </List.Accordion>
    <List.Accordion
                 title="What happens once my debt is paid off?"
                 titleStyle={accordionStyle.sectionTitle}
                 titleNumberOfLines={numTitleLines}>
                 <Text style={accordionStyle.sectionDescription}>
                 Once you complete the program you will receive a letter of completion from Take Charge America.
                 You will also want to confirm your creditor statements and credit report reflect a paid in full status for each account.
                 Please continue to visit the Take Charge America Education Library to access new blogs,
                 Q&As and personal finance calculators that can help you pay off any other debts, save money and balance your budget.

                 </Text>
               </List.Accordion>
    </List.Section>
        <Text style={commonStyle.sectionHeader}>Contact Us</Text>

        <Text style={accordionStyle.hotlineTitle}>Payment Questions</Text>
        <Text onPress={()=>{Linking.openURL('tel:800-823-7396');}} style={accordionStyle.contactInfo}>800-823-7396</Text>

        <Text style={accordionStyle.hotlineTitle}>Update Contact Info</Text>
        <Text onPress={()=>{Linking.openURL('tel:800-823-7396');}} style={accordionStyle.contactInfo}>800-823-7396</Text>

        <Text style={accordionStyle.hotlineTitle}>Budget Updates</Text>
        <Text onPress={()=>{Linking.openURL('tel:800-823-7396');}} style={accordionStyle.contactInfo}>800-823-7396</Text>

        <Text style={accordionStyle.hotlineTitle}>Student Loan Support</Text>
        <Text onPress={()=>{Linking.openURL('tel:877-784-2008');}} style={accordionStyle.contactInfo}>877-784-2008</Text>

        <TouchableOpacity onPress={() => Linking.openURL('http://takechargeamerica.org/studentloans')}>
                <Text style={accordionStyle.link}>Learn More</Text></TouchableOpacity>

        <Text style={accordionStyle.hotlineTitle}>Housing Help</Text>
        <Text onPress={()=>{Linking.openURL('tel:877-784-2008');}} style={accordionStyle.contactInfo}>877-784-2008</Text>
        <TouchableOpacity onPress={() => Linking.openURL('http://takechargeamerica.org/housing-help')}>
        <Text style={accordionStyle.link}>Learn More</Text></TouchableOpacity>

        <Text style={accordionStyle.hotlineTitle}>Bankruptcy Resources</Text>
        <Text onPress={()=>{Linking.openURL('tel:866-750-9634');}} style={accordionStyle.contactInfo}>866-750-9634</Text>
        <TouchableOpacity onPress={() => Linking.openURL('http://takechargeamerica.org/bankruptcy')}>
        <Text style={accordionStyle.link}>Learn More</Text></TouchableOpacity>

    </ScrollView>
    <FooterComponent navigation={this.props.navigation}/>
    </SafeAreaView>
</>

  );
  }
};