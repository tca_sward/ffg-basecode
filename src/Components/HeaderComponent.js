import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  Linking,
  TouchableOpacity,
} from 'react-native';
import { Navigationbody } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { FontAwesome } from '@expo/vector-icons';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


const HeaderComponent = (props) => {
    return (
        <View style={styles.body}>
            <Image source={require('../assets/images/TCABanner.png')} style={styles.bannerImg} />
            <TouchableOpacity onPress={() => { props.navigation.toggleDrawer()}} style={styles.menuBox}>
                <Text style={{marginTop: 0, fontFamily: 'condensed', fontSize: font18, fontWeight: '400'}}>MENU</Text>
                <FontAwesome name='bars' style={[commonStyle.icons, {margin: 0, marginLeft: 10, fontSize: 18}]}/>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    bannerImg: {
        width: 300,
        height: 50,
        padding: 10,
    },
    body: {
        marginTop: ios ? 0 : 15,
        marginBottom: 20,
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    menuBox: {
        marginTop: 20,
        marginLeft: 20,
        flexDirection: 'row',
    },
});


export default HeaderComponent;