import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, Image, ScrollView, TextInput, Button, TouchableOpacity } from 'react-native';
import image from 'react-native'
import { FontAwesome } from '@expo/vector-icons';
import { DottedLine, commonStyle, HeaderLogoImage, moderateScale, font13, font15, font18, font20, font22, white } from '../../style/CommonStyle.js';

const PaymentProgressHeader = (props) => {
  return (
    <View>
        <View style={[commonStyle.LeftAndRight, {flexDirection: 'row', marginBottom: 20}]}>
            <Text style={commonStyle.sectionHeader}>Payment Progress</Text>
            <TouchableOpacity style={{position: 'absolute', right: 0}} onPress={() => { props.navigation.navigate('ProgressReport', {accounts: props.accounts, total: props.total, progress: props.progress, date: props.date})}}>
                <FontAwesome name='search' style={[commonStyle.fontAwesomeStyle, {position: 'absolute', right: 110}]}/>
                <Text style={[commonStyle.sectionRight,{width: '100%', right: -15}]}>See full report</Text>
            </TouchableOpacity>
        </View>
        <DottedLine/>
    </View>
  );
};


export default PaymentProgressHeader;
