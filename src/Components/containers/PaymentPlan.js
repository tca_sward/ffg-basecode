import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, Image, ScrollView, TextInput, Button, StyleSheet } from 'react-native';
import image from 'react-native'
import { FontAwesome } from '@expo/vector-icons';
import { DottedLine, commonStyle, HeaderLogoImage, moderateScale, font13, font15, font18, font20, font22, white } from '../../style/CommonStyle.js';

const PaymentPlanHeader = (props) => {
  return (
    <View>
      <View style={[commonStyle.LeftAndRight, {flexDirection: 'row', marginBottom: 20}]}>
          <Text style={commonStyle.sectionHeader}>Payment Plan</Text>
          <DottedLine/>
          <FontAwesome name='print' style={[commonStyle.fontAwesomeStyle, {position: 'absolute', right: 55}]}/>
          <Text style={[commonStyle.sectionRight, {position: 'absolute', right: 10}]}>Print</Text>
      </View>
      <DottedLine/>
    </View>
  );
};

export default PaymentPlanHeader;