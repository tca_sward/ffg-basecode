import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, TextInput, Button, StyleSheet } from 'react-native';
import image from 'react-native'
import { FontAwesome, FontAwesome5 } from '@expo/vector-icons';

const MonthlyStatementsTouchable = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.row}>
                <View style={styles.subContainer}>
                    <Text style={{ textAlign: 'left', fontSize: 22, textTransform: 'uppercase' }}>Monthly{'\n'}Statements</Text>
                </View>
                <View style={{flexDirection: 'column', position: 'absolute', right: 10, top: 20}}>
                    <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => props.navigation.navigate('MonthlySummary', {openCurrentMonth: true})}>
                        <FontAwesome5 name='clock' style={styles.icons}/>
                        <Text style={styles.blueText}>CURRENT </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection: 'row', marginTop: 10}} onPress={() => props.navigation.navigate('MonthlySummary', {openCurrentMonth: false})}>
                        <FontAwesome name='calendar-o' style={styles.icons}/>
                        <Text style={styles.blueText}>FULL HISTORY</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        bottom: 0,
        justifyContent: 'center',
        paddingBottom: Platform.OS === 'ios' ? 0 : 15,
        paddingTop: 20,
        marginLeft: 30,
        marginRight: 30,
    },
    subContainer: {
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
        paddingBottom: Platform.OS === 'ios' ? 0 : 15,
        paddingTop: 20,
    },
    row: {
        flexDirection: 'row'
    },
    blueText: {
        fontSize: 15,
        color: 'rgba(1,74,127,255)',
        fontWeight: 'bold'
    },
    iconImg: {
        width: 20,
        height: 20,
        padding: 10,
    },
    greyText: {
        color: 'rgba(228, 233, 237, 1)'
    },
    icons:{
        marginRight: 5,
        fontSize: 20,
        color:'#43b7d7',
    },
})

export default MonthlyStatementsTouchable;