import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, TextInput, Button, Platform, StyleSheet, Dimensions, PixelRatio } from 'react-native';
import image from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons';

const PaymentHistoryTouchable = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.row}>
                <View style={styles.subContainer}>
                    <Text style={{ textAlign: 'left', fontSize: 22, textTransform: 'uppercase' }}>Payment{'\n'}History</Text>
                </View>
                <View style={{position: 'absolute', right: 20, top: 40}}>
                    <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => props.navigation.navigate('PaymentHistory')}>
                        <FontAwesome5 name='glasses' style={styles.icons}/>
                        <Text style={styles.blueText}>READ MORE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const glassImage = { uri: 'https://cdn4.iconfinder.com/data/icons/medicons-2/512/glasses-512.png' }

const styles = StyleSheet.create({
    container: {
        bottom: 0,
        justifyContent: 'center',
        paddingBottom: Platform.OS === 'ios' ? 0 : 15,
        paddingTop: 20,
        marginLeft: 30,
        marginRight: 30,
    },
    subContainer: {
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
        paddingBottom: Platform.OS === 'ios' ? 0 : 15,
        paddingTop: 20,
    },
    row: {
        flexDirection: 'row'
    },
    blueText: {
        fontSize: 15,
        color: 'rgba(1,74,127,255)',
        fontWeight: 'bold'
    },
    icons:{
        marginRight: 5,
        fontSize: 20,
        color:'#43b7d7',
    },
    greyText: {
        color: 'rgba(228, 233, 237, 1)'
    }
})

export default PaymentHistoryTouchable;