import React, {Component} from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View, Button, TouchableOpacity, Image, Platform } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { FontAwesome } from '@expo/vector-icons';
import Moment from 'moment';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class RescheduleNextPayment extends Component {

    constructor(props: Props) {
        super(props);
        this.state = {
            activeSections: [],
            nextPaymentDateOriginal: new Date(),
            nextPaymentDateNew: new Date(),
            showCalendar: false,
            paymentDateChange: false,
            minimumDate: new Date(Moment().add(1, 'days')), //this.props.minimumDate
            maximumDate: new Date(Moment().add(2, 'months')), //this.props.maximumDate
          };
    }


    render() {
        return (
            <View style={{marginLeft: -20, marginTop: ios ? 30:0, alignItems: 'center'}}>
                <Text style={commonStyle.blueLabel}>Next Payment Date</Text>
                <Text style={commonStyle.largeNumberText}>{Moment(this.state.nextPaymentDateOriginal).format('MM/DD/YYYY')}</Text>
                <View style={{flexDirection: 'column'}}>
                    <Text style={commonStyle.blueLabel}>Reschedule To</Text>
                    <TouchableOpacity style={{marginTop: 10, marginBottom: 30, alignSelf: 'center'}} onPress={() => { this.setState({showCalendar: !this.state.showCalendar}) }}>
                        <Text style={{fontSize: font18, color: '#403d3d'}}>
                        {this.state.paymentDateChange ? formatDate(this.state.nextPaymentDateNew):'MM/DD/YYYY'}
                        </Text>
                    </TouchableOpacity>
                </View>
                {this.state.showCalendar &&
                    <DateTimePicker
                    style={{width: 320, backgroundColor: "white"}}
                    testID="dateTimePicker"
                    display={ios ? "spinner":"calendar"}
                    value={this.state.nextPaymentDateNew}
                    mode="date"
                    format="MM/DD/YYYY"
                    minimumDate={this.state.minimumDate}
                    maximumDate={this.state.maximumDate}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    onChange={(event, date) =>{this.setState({showCalendar: ios? this.state.showCalendar:!this.state.showCalendar, paymentDateChange: true, nextPaymentDateNew: date})}}
                    />
                }
                <TouchableOpacity title="Reschedule Payment" style={{marginTop: 20, padding: 12, width: 350, backgroundColor: '#1e66bd', borderRadius: 20, borderWidth: 0, borderColor: 'white'}} onPress={() => { this.props.navigation.navigate('EmptyPage')}}>
                    <Text style={{alignSelf: 'center', fontSize: font18, color: 'white', fontWeight: 'bold', textTransform: 'uppercase'}}>Reschedule Payment</Text>
                </TouchableOpacity>
            </View>
        );
    }
}


const formatDate = (date) => {
  return (
    Moment(date).format('MM/DD/YYYY')
  );
};