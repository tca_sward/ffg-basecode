import React, { useState } from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  View,
  Text,
  StatusBar,
  Divider,
  Image,
  Linking,
  FlatList,
  Alert,
  Modal,
  Pressable,
} from 'react-native';
import { List } from 'react-native-paper';
import ScheduledPaymentTile from './ScheduledPaymentTile.js';
import accordionStyle from '../style/AccordionStyle.js';
import { commonStyle } from '../style/CommonStyle.js';
import currentPaymentStyle from '../style/CurrentPaymentStyle.js';

//TODO: ViewScheduledPayments is just to demo the component (ScheduledPaymentTile) and needs to be integrated with the other MakeAPayment pages

const ViewScheduledPayments = () => {
const numTitleLines = 3;
const FeatureUnavailablePopUp = () => {
    const [modalVisible, setModalVisible] = useState(false);
          return (
        <View style={[currentPaymentStyle.centeredView, {justifyContent: 'center', flexDirection: 'column', flex: 1}]}>
          <Modal
            animationType="none"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible(!modalVisible);
            }}
          >
            <View style={currentPaymentStyle.modalView}>
                <Pressable
                    style={[currentPaymentStyle.xButtonStyle]}
                    onPress={() => setModalVisible(!modalVisible)}
                    >
                    <Text style={currentPaymentStyle.xButtonText}>X</Text>
                </Pressable>
            <View style={currentPaymentStyle.centeredView}>
                <Text style={currentPaymentStyle.fieldLabel}>This feature is only available for clients who pay monthly.
                If you would like to change to monthly payments, please call a counselor at 800-823-7396.</Text>
              </View>
            </View>
          </Modal>

          <Pressable
            style={[currentPaymentStyle.cancelButton]}
            onPress={() => setModalVisible(true)}
          >
            <Text style={currentPaymentStyle.cancel}>Not Qualified</Text>
          </Pressable>
        </View>
        );
};

  return (
  <>
    <SafeAreaView style={[commonStyle.fullPageContainer, {height: 'auto'}]}>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
            <View style={currentPaymentStyle.paymentView}>
                <ScheduledPaymentTile paymentDate='12/13/2020' paymentAmount='50' isAutoPay={true} ></ScheduledPaymentTile>
                <ScheduledPaymentTile paymentDate='12/24/2020' paymentAmount='100.80' isAutoPay={false} canCancel={true}></ScheduledPaymentTile>
                <ScheduledPaymentTile paymentDate='1/1/2021' paymentAmount='1000.80' isAutoPay={false}  canCancel={false}></ScheduledPaymentTile>
                <ScheduledPaymentTile paymentDate='11/11/2021' paymentAmount='120.80' isAutoPay={true} ></ScheduledPaymentTile>
             </View>
        </ScrollView>
    </SafeAreaView>

    </>

  );
};

export default ViewScheduledPayments;