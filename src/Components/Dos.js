/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow strict-local
 * @format
 */

import type {Node} from 'react';
import {Platform, StyleSheet, Text} from 'react-native';
import React from 'react';
import { commonStyle, darkBlueText, moderateScale } from '../style/CommonStyle.js';

const styles = StyleSheet.create({
  highlight: {
    fontFamily: 'bold',
    color: darkBlueText,
  },
});

const Dos = (): Node => (
    <Text>
      <Text style={styles.highlight}>Do</Text> Pay every account on time, every time {'\n\n'}
      <Text style={styles.highlight}>Do</Text> Pay more than the minimum amount due every month {'\n\n'}
      <Text style={styles.highlight}>Do</Text> Work on getting credit card balances below 30% of
      total credit limit {'\n\n'}
      <Text style={styles.highlight}>Do</Text> Make extra payments whenever possible {'\n\n'}
      <Text style={styles.highlight}>Do</Text> Use unexpected money, such as gifts and bonuses, to
      pay down debt {'\n\n'}
      <Text style={styles.highlight}>Do</Text> Build an emergency fund to minimize reliance on
      credit cards {'\n\n'}
      <Text style={styles.highlight}>Do</Text> Start planning for life after debt
    </Text>

  );

export default Dos;
