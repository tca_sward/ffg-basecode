import React, {Component} from 'react';
import { Text, View, SafeAreaView } from 'react-native';

import HeaderComponent from './HeaderComponent';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class EmptyPage extends Component {
    render() {
        return (
            <SafeAreaView style={{backgroundColor: 'white'}}>
                <HeaderComponent navigation={this.props.navigation}/>
                <View style={{height: '100%', margin: ios ? 50:0}}>
                    <Text>Empty Page - Placeholder</Text>
                </View>
            </SafeAreaView>
        );
    }
}

