import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


const FooterComponent = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => { props.navigation.navigate('PaymentPlan')}} style={styles.navElement}>
                <Image source={require('../assets/images/paymentPlanIcon.png')} style={{width: 20, height: 20, marginBottom: 10}} />
                <Text style={[commonStyle.navigationItems, styles.navText]}>Payment{'\n'}Plan</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { props.navigation.navigate('EmptyPage')}} style={styles.navElement}>
                <FontAwesome name='calendar' style={[commonStyle.icons, styles.icons]}/>
                <Text style={[commonStyle.navigationItems, styles.navText]}>Monthly{'\n'}Budget</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { props.navigation.navigate('CreditSummary')}} style={styles.navElement}>
                <FontAwesome name='list-ul' style={[commonStyle.icons, styles.icons]}/>
                <Text style={[commonStyle.navigationItems, styles.navText]}>Credit{'\n'}Summary</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { props.navigation.navigate('EducationCenter')}} style={styles.navElement}>
                <FontAwesome name='graduation-cap' style={[commonStyle.icons, styles.icons]}/>
                <Text style={[commonStyle.navigationItems, styles.navText]}>Education{'\n'}Center</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
     container: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        backgroundColor: 'blue',
        paddingTop: 10,
        paddingBottom: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'white',
        paddingBottom: '5%',
        paddingTop: '2%'
     },
     icons: {
        margin: 0, 
        marginBottom: 10
     },
     navElement: {
         flexDirection: 'column',
         alignItems: 'center',
         marginLeft: 20,
         marginRight: 20,
      },
     navText: {
         textAlign: 'center',
         fontSize: font15,
     },
});

export default FooterComponent;