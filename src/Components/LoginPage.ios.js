import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, Image, ScrollView, TextInput, Button, commonStyleheet, Platform, Linking } from 'react-native';
import image from 'react-native'
import CommonStyle, { commonStyle } from '../style/CommonStyle';

const LoginPage = (props) => {
    return (
        <View style={{backgroundColor: 'white'}}>
            <Image source={require('../assets/images/TCABanner.png')}
                            style={{ marginTop: 50, marginBottom: 80, width: 400, height: 70 }} />
            <View style={commonStyle.container}>
                <Text style={commonStyle.standardText}>          Email<Text style={commonStyle.redText}>*</Text></Text>
                <TextInput
                    style={commonStyle.input}
                />
                <Text>{"\n"}</Text>
                <Text style={commonStyle.standardText}>          Password<Text style={commonStyle.redText}>*</Text><Text style={commonStyle.smallText} onPress={() => Linking.openURL('https://mytca.org/Home/ForgotPassword')}>                                                  Forgot Password?</Text></Text>
                <TextInput
                    style={commonStyle.input}
                />
            </View>
            <View style={commonStyle.loginButtonSection}>
                < Button
                    title="Log In "
                    raised={true}
                    color='white'
                />
            </View>
            <View style={{marginBottom: 120}}>
                <Text style={commonStyle.centerTextBold}> New To MyTCA?</Text>
                <Text style={commonStyle.underlineCenterText} onPress={() => Linking.openURL('https://mytca.org/Home/Registration')}>Register Here</Text>
                <Text>{"\n"}</Text>
                <Text style={commonStyle.centerTextBold}>Having trouble signing in?</Text>
                <Text style={commonStyle.centerText}>Call a counselor at</Text>
                <Text onPress={()=>{Linking.openURL('tel:800-823-7396');}} style={commonStyle.centerText}>800-823-7396</Text>
                <Text style={commonStyle.centerText}>Mon - Fri 6am - 6pm</Text>
                <Text>{"\n"}</Text>
            </View>
            <View style={[commonStyle.containerBlack]}>
                <Text style={commonStyle.centerTextSmall}>Copyright © 2021 Take Charge America, Inc.</Text>
                <Text style={commonStyle.centerTextSmall}>20620 North 19th Avenue, Phoenix, Arizona 85027</Text>
                <Text style={commonStyle.centerTextSmall}>State Disclosures | Terms of Use | Consumer Privacy Policy |</Text>
                <Text style={commonStyle.centerTextSmall}>Online Privacy Policy | Partners</Text>

                <Text style={[commonStyle.centerTextSmall, {marginTop: 10}]}>501 (c) (3) Nonprofit Financial Education. | All rights reserved.</Text>
            </View>
        </View>
    );
};


export default LoginPage;