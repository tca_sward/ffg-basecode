import React, {useState, setState, useEffect} from 'react';
import { Text, View, TextInput, ScrollView, SafeAreaView, KeyboardAvoidingView, StyleSheet } from 'react-native';
import curvedTileStyle from '../style/CurvedTileStyle';
import { DottedLine, commonStyle, itemColor, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue} from  '../style/CommonStyle';

import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import PaymentProgress from './PaymentProgress.js';
import Account from './Account.js';
import DropDownPicker from 'react-native-dropdown-picker';


const ProgressReport = (props) => {

    const fullAccountList = props.accounts

    var [partialAccounts, setPartialAccounts] = useState(props.route.params.accounts)
    var [searchInput, setSearchInput] = useState("")
    var [sortSelection, setSortSelection] = useState("")

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        {"label": "", "value": ""},
        {"label": "Remaining Amount (High-Low)", "value": "highLow"},
        {"label": "Remaining Amount (Low-High)", "value": "lowHigh"},
        {"label": "Name (A-Z)", "value": "name"}
    ]);

    useEffect(() => {
        sortAccounts()
    },[sortSelection])

    useEffect(() => {
        setSortSelection(value)
    },[value])

    const searchAccounts = () => {
        var tempList = []
        if (searchInput == null) {
            setPartialAccounts(props.route.params.accounts)
        } else {
            props.route.params.accounts.map(element => {
                if (element["name"].toUpperCase().includes(searchInput.toUpperCase())) {
                    tempList.push(element)
                }
            });
            setPartialAccounts(tempList)
        }
    }

    const sortAccounts = () => {
        var tempList = partialAccounts.map(a => ({...a}));
        setPartialAccounts([])
        if (sortSelection === "name") {
            tempList = tempList.sort(function(a, b) {
                var textA = a.name.toUpperCase();
                var textB = b.name.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else if (sortSelection == "highLow") {
            tempList = tempList.sort(function(a, b) {
                var textA = a["Remaining Balance"];
                var textB = b["Remaining Balance"];
                return (textA > textB) ? -1 :(textA > textB) ? 1 : 0;
            });
        } else if (sortSelection == "lowHigh") {
            tempList = tempList.sort(function(a, b) {
                var textA = a["Remaining Balance"];
                var textB = b["Remaining Balance"];
                return (textA < textB) ? -1 :(textA < textB) ? 1 : 0;
            });
        }
        setPartialAccounts(tempList)
    }

    return (

        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
            <HeaderComponent navigation={props.navigation}/>
            <ScrollView style={commonStyle.scrollViewBody}>
                <View style={commonStyle.LeftAndRight}>
                    <Text style={commonStyle.sectionHeader}> PROGRESS REPORT </Text>
                </View>
                <View>
                    <PaymentProgress
                        accounts={props.route.params.accounts}
                        total={props.route.params.total}
                        progress={props.route.params.progress}
                        date={props.route.params.date}></PaymentProgress>
                </View>
                <View style={commonStyle.LeftAndRight}>
                    <Text style={commonStyle.sectionHeader}>
                        PROGRESS PER ACCOUNT
                    </Text>
                </View>
                <DottedLine></DottedLine>
                <View style={{bottomMargin: '10%', zIndex: 200}}>
                    <Text style={commonStyle.fieldLabel}>Sort By</Text>
                    <View style={{zIndex: 200, bottomMargin: '10%'}}>
                        <DropDownPicker selectedValue={sortSelection}
                                style={commonStyle.pickerStyle}
                                open={open}
                                value={value}
                                items={items}
                                setOpen={setOpen}
                                setValue={setValue}
                                setItems={setItems}
                                >
                        </DropDownPicker>
                    </View>
                    <Text style={commonStyle.fieldLabel}>Search</Text>
                    <View >
                        <TextInput
                            style={commonStyle.textInput}
                            onChangeText={searchInput => setSearchInput(searchInput)}
                            onEndEditing={searchAccounts}
                            defaultValue={searchInput}
                            placeholder="Enter Search Criteria"
                        />
                    </View>
                </View>
                <View>
                    {
                        partialAccounts.map((index, element) => {
                            return (<Account
                                        bank_name={index["name"]}
                                        remaining_balance={index["Remaining Balance"]}
                                        original_balance={index["Original Balance"]}
                                    ></Account>);
                        })
                    }
                </View>
            </ScrollView>
            <FooterComponent navigation={props.navigation}/>
        </SafeAreaView>
  );
};

const progressReportStyle = StyleSheet.create({
    pickerStyle: {
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        borderBottomLeftRadius: 30,
        borderTopLeftRadius: 30,
        marginTop: "1%",
        marginBottom: "2%",
        paddingLeft: '5%',
        paddingTop: '2%',
        paddingBottom: '2%',
        borderColor: '#43b7d7',
        backgroundColor: '#ecf0f1',
        color: 'gray'
    }
})


export default ProgressReport;
