import React, {Component} from 'react';
import 'react-native-gesture-handler';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, Image, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import Collapsible from 'react-native-collapsible';
import { FontAwesome } from '@expo/vector-icons';
import Moment from 'moment';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import PaymentProgress from './PaymentProgress';
import NextPayment from './NextPayment';
import MakeAPaymentStep1 from './MakeAPaymentStep1';
import RescheduleNextPayment from './RescheduleNextPayment';
import MonthlySummary from './MonthlySummary';
import MonthlyStatementsTouchable from './containers/MonthlyStatements';
import PaymentHistoryTouchable from './containers/PaymentHistory';
import PaymentProgressHeader from './containers/PaymentProgress';
import PaymentPlanHeader from './containers/PaymentPlan';
import ViewScheduledPayments from './ViewScheduledPayments';

import { DottedLine, commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue} from  '../style/CommonStyle';


export default class PaymentPlan extends Component {

    constructor(props: Props) {
        super(props);
        this.state = {
            MakeAPaymentCollapsed: true,
            RescheduleNextPaymentCollapsed: true,
            ViewScheduledPaymentsCollapsed: true,
            accounts: [{'name': 'Discover', 'Remaining Balance': 123.00, 'Original Balance': 500.00}, {'name': 'American Express', 'Remaining Balance': 5000.00, 'Original Balance': 5000.00}],
            paymentProgressInfo: { total: 28207.03, progress: .3, date: '11/15/2021'},
            makeAPaymentValues: { paymentTypeSelected: 'Monthly',
                                 paymentAmountMonthly: 25,
                                 paymentAmountSelected: 0,
                                 paymentDateSelected: new Date(Moment().add(3, 'days')),
                                 paymentDateMinimum: new Date(Moment().add(3, 'days')),
                                 paymentDateMaximum: new Date(Moment().add(2, 'years')),
                                 paymentDateChange: false,
                                 accountSelected: '',
                                 accountSelectedMonthly: 'Chase Bank #1234',
                                 accounts: ['No Preference', 'Chase Bank #1234','Discover Card #5678'],
                                 showCalendar: false,
                                 paymentTypeDropdownOpen: false,
                                 accountSelectedDropdownOpen: false,
                                 refreshed: true
                               }
        }

        this.defaultState = JSON.parse(JSON.stringify(this.state));

        this.props.navigation.addListener(
            'focus',
            payload => {
                if (this.props != null && this.props['route'] != null && this.props.route['params'] != null) {
                    if (this.props.route.params['ViewScheduledPaymentsCollapsed'] != null) {
                        this.openViewScheduledPaymentsCollapsed();
                        this.setState({
                            ViewScheduledPaymentsCollapsed: this.props.route.params['ViewScheduledPaymentsCollapsed']
                        });
                        this.props.navigation.setParams({'ViewScheduledPaymentsCollapsed': null})

                    }
                    if (this.props.route.params['MakeAPaymentCollapsed'] != null) {
                        this.openMakeAPaymentCollapsed();
                        this.setState({
                            MakeAPaymentCollapsed: this.props.route.params['MakeAPaymentCollapsed']
                        });
                        this.props.navigation.setParams({'MakeAPaymentCollapsed': null})
                    }
                }
            }
        );
    }

    makeAPaymentCollapseCallback = (collapseBoolean) =>{
        this.setState({MakeAPaymentCollapsed: collapseBoolean})
    }

    openMakeAPaymentCollapsed = () => {
       this.setState ({
           RescheduleNextPaymentCollapsed: true,
           ViewScheduledPaymentsCollapsed: true,
       });
    }

    openRescheduleNextPaymentCollapsed = () => {
      this.setState ({
          MakeAPaymentCollapsed: true,
          ViewScheduledPaymentsCollapsed: true,
      });
    }

    openViewScheduledPaymentsCollapsed   = () => {
        this.setState ({
            MakeAPaymentCollapsed: true,
            RescheduleNextPaymentCollapsed: true,
        });
    }


    render() {

        const solidLine = () => (
            <View style={{width: 360, borderBottomColor: '#8f8d8b', opacity: .8, borderBottomWidth: 2}}/>
        )

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <HeaderComponent navigation={this.props.navigation}/>
                <ScrollView style={commonStyle.scrollViewBody}>
                    <View>
                        <View style={{marginRight: 20}}>
                            <PaymentProgressHeader navigation={this.props.navigation} accounts={this.state.accounts} total={this.state.paymentProgressInfo["total"]} progress={this.state.paymentProgressInfo["progress"]}  date={this.state.paymentProgressInfo["date"]}/>
                            <PaymentProgress navigation={this.props.navigation} accounts={this.state.accounts} total={this.state.paymentProgressInfo["total"]} progress={this.state.paymentProgressInfo["progress"]}  date={this.state.paymentProgressInfo["date"]}/>
                        </View>
                        <View style={{marginTop: 20, marginRight: 10}}>
                            <PaymentPlanHeader />
                            <NextPayment />
                        </View>
                        {solidLine()}
                        <View style={[styles.collapseContainers, {marginTop: 30}]}>
                            <TouchableOpacity onPress={() => { this.openMakeAPaymentCollapsed(); this.setState({MakeAPaymentCollapsed:!this.state.MakeAPaymentCollapsed}) }}>
                                <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.collapseLabels}>Make A Payment</Text>
                                        <FontAwesome name='chevron-right' style={[commonStyle.icons, styles.carat]}/>
                                </View>
                            </TouchableOpacity>
                            <Collapsible collapsed={this.state.MakeAPaymentCollapsed}>
                                <MakeAPaymentStep1 navigation={this.props.navigation} makeAPaymentCollapseCallback={this.makeAPaymentCollapseCallback} makeAPaymentValues={this.state.makeAPaymentValues}/>
                            </Collapsible>
                        </View>
                        {solidLine()}
                        <View style={styles.collapseContainers}>
                            <TouchableOpacity onPress={() => { this.openRescheduleNextPaymentCollapsed(); this.setState({RescheduleNextPaymentCollapsed:!this.state.RescheduleNextPaymentCollapsed}) }}>
                                <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.collapseLabels}>Reschedule Next Payment</Text>
                                    <FontAwesome name='chevron-right' style={[commonStyle.icons, styles.carat]}/>
                                </View>
                            </TouchableOpacity>
                            <Collapsible collapsed={this.state.RescheduleNextPaymentCollapsed}>
                                <RescheduleNextPayment navigation={this.props.navigation}/>
                            </Collapsible>
                        </View>
                        {solidLine()}
                        <View style={styles.collapseContainers}>
                            <TouchableOpacity onPress={() => { this.openViewScheduledPaymentsCollapsed(); this.setState({ViewScheduledPaymentsCollapsed:!this.state.ViewScheduledPaymentsCollapsed}) }}>
                                <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.collapseLabels}>View Scheduled Payments</Text>
                                    <FontAwesome name='chevron-right' style={[commonStyle.icons, styles.carat]}/>
                                </View>
                            </TouchableOpacity>
                            <Collapsible collapsed={this.state.ViewScheduledPaymentsCollapsed}>
                                <ViewScheduledPayments />
                            </Collapsible>
                        </View>
                        {solidLine()}
                        <View>
                            <PaymentHistoryTouchable navigation={this.props.navigation}/>
                            <View style={{position: 'relative', top: 100}}>
                                <DottedLine/>
                            </View>
                        </View>
                        <View style={{marginTop: 80, marginBottom: 120}}>
                            <MonthlyStatementsTouchable navigation={this.props.navigation}/>
                            <View style={{position: 'relative', top: 100}}>
                                <DottedLine/>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <FooterComponent navigation={this.props.navigation}/>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    carat: {
        right: 30,
        position: 'absolute',
        margin: 0,
        marginRight: 20,
    },
    collapseContainers: {
        marginTop: 30,
        marginBottom: 30,
    },
    collapseLabels: {
        fontSize: font20,
        fontWeight: 'bold'
    },
    iconImg: {
        width: 20,
        height: 20,
        padding: 10,
    },
    label: {
        marginLeft: marginLeftBody,
        marginBottom: 10,
        fontSize: font18,
        color: darkBlue,
        fontWeight: '600',
        textTransform: 'uppercase',
    },
});




