import React, {Component} from 'react';
import 'react-native-gesture-handler';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, Image  } from 'react-native';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class MyAccountPage extends Component {

    constructor(props: Props) {
        super(props);
        this.state = {
            clientInfo: {text: '', firstName: 'John', lastName: 'Williams', clientId: '11636220',
                         address: '2205 Swift Bluff Dr', address2: '', city: 'Colonial Heights', usState: 'VA',
                         zipCode: '23834', phone: '804-720-0280', type: 'Cell Phone', email: 'jdwilliams002@gmail.com',} //this.props.clientInfo;
         };
    }


    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <HeaderComponent navigation={this.props.navigation}/>
                <ScrollView style={commonStyle.scrollViewBody}>
                    <View>
                        <View style={{flexDirection: 'row', marginBottom: 20}}>
                            <Text style={[commonStyle.title, {textTransform: 'uppercase'}]}>My Account</Text>
                            <Text style={{marginLeft: 45, marginTop: 15, textDecorationLine: 'underline', fontSize: font15, color: 'blue', textTransform: 'uppercase'}}
                              onPress={() => {  this.props.navigation.navigate('NewPassword') }}>
                              Change Password
                            </Text>
                        </View>
                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>First Name</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.firstName}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>Last Name</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.lastName}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>Client Id</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.clientId}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>Address</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.address}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>Address 2</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.address2}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>City</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.city}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>State</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.usState}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>Zip Code</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.zipCode}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>Phone</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.phone}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>Type</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.type}</Text>

                        <Text style={[commonStyle.blueLabel, styles.fieldLabel]}>Email</Text>
                        <Text style={styles.fieldEntry}>{this.state.clientInfo.email}</Text>
                    </View>
                </ScrollView>
                <FooterComponent navigation={this.props.navigation}/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    fieldLabel: {
        marginLeft: marginLeftBody,
        marginBottom: 5,
        fontSize: font20,
        fontWeight: '600',
    },
    fieldEntry: {
        marginLeft: marginLeftBody,
        marginBottom: 10,
        fontSize: font18,
    },
    iconImg: {
        width: 20,
        height: 20,
        padding: 10,
    },
});
