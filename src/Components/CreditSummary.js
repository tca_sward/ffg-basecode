import React, {Component} from 'react';
import 'react-native-gesture-handler';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, Image, TouchableOpacity } from 'react-native';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import Dos from './Dos';
import Donts from './Donts';
import WhatAffectsYourCreditScore from './WhatAffectsYourCreditScore';
import CurvedTile from './CurvedTile';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';
export default class CreditSummary extends Component {

    constructor(props: Props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <HeaderComponent navigation={this.props.navigation}/>
                <ScrollView style={[commonStyle.scrollViewBody, {marginLeft: 0}]}>
                    <Text style={{marginLeft: 40, marginRight: ios ? 60:45, fontSize: 25}}>What Affects Your {'\n'}Credit Score?</Text>
                    <View style={{marginLeft: 10, marginRight: 10}}>
                        <CurvedTile title='Payment History' percent='35' description='Includes on-time payments, collection accounts, charge-offs, tax liens, foreclosures and bankruptcy.' />
                        <CurvedTile title='Debt Levels' percent='30' description='Includes overall debt, the ratio of credit card balances to credit limit, and loan balances to original loan amounts.' />
                        <CurvedTile title='New Credit' percent='10' description='Includes the age of your oldest credit account and an average age of all accounts.' />
                        <CurvedTile title='Credit Age' percent='15' description='Includes the age of your oldest credit account and an average age of all accounts.' />
                        <CurvedTile title='Credit Mix' percent='10' description='Includes revolving accounts, such as credit cards and installments accounts, like vehicle or student loans.' />
                    </View>
                    <WhatAffectsYourCreditScore />
                </ScrollView>
                <FooterComponent navigation={this.props.navigation}/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
});