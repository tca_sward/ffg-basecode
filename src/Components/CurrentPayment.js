import React from 'react';
import { Text, View, ScrollView} from 'react-native';
import currentPaymentStyle from '../style/CurrentPaymentStyle.js';
import { commonStyle } from '../style/CommonStyle.js';

const CurrentPayment = () => {
  return (
    <View>
        <ScrollView>
            <View style={currentPaymentStyle.imageStyle}>
                <Text style={currentPaymentStyle.whiteTextStyle}>
                    Monthly Payment - Autopay
                </Text>
            </View>
            <View style={commonStyle.roundedRectangleBottomStyle}>
                <View>
                    <Text style={commonStyle.paragraphText}>Payment Date</Text>
                </View>  
                <View>
                    <Text style={commonStyle.largeNumberText}>03/26/2021</Text>
                </View>  
                <View>
                    <Text style={commonStyle.paragraphText}>Payment Amount</Text>
                </View>  
                <View>
                    <Text style={commonStyle.largeNumberText}>$9,999</Text>
                </View>  
            </View>
        </ScrollView>
    </View>
  );
};

export default CurrentPayment;