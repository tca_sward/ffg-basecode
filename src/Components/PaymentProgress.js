import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Linking, Dimensions, Platform, PixelRatio, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import { FontAwesome } from '@expo/vector-icons';
import { Tooltip } from 'react-native-elements';
import paymentProgressStyle from '../style/PaymentProgressStyle.js';
import { ProgressBar, Colors } from 'react-native-paper';
import { DottedLine, commonStyle, HeaderLogoImage, moderateScale, font13, font15, font18, font20, font22, white } from '../style/CommonStyle.js';

const PaymentProgress = (props) => {
    return (
    <View>
          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
              <Text style={[commonStyle.blueLabel, {alignSelf: 'center', marginTop: 20}]}>Estimated Remaining Debt</Text>
              <Tooltip withOverlay={false} 
                       backgroundColor={'white'} 
                       width={180} 
                       height={100} 
                       containerStyle={{top: 100, left: 220, padding: 5, borderRadius: 0, borderWidth: '.3px', borderColor: '#A8A8A8'}} 
                       popover={<Text style={{color: '#777778', fontSize: font13}}>This is an estimate. Please refer to your monthly creditor statements for the most up-to-date balance information.</Text>}>
                    <FontAwesome name='question-circle' style={[commonStyle.fontAwesomeStyle, {marginTop: 22, marginLeft: 5}]}/>
              </Tooltip>
          </View>
          <View style={paymentProgressStyle.genericPadding}>
            <Text style={commonStyle.largeNumberText}>${props.total}</Text>
          </View>
          <View style={paymentProgressStyle.aboveProgressBarStyle}>
            <ProgressBar style={paymentProgressStyle.progressBarStyle} progress={props.progress} color={'#43b7d7'}/>
          </View>
          <View style={{alignSelf: 'center'}}>
            <Text style={commonStyle.blueLabel}>Estimated Debt Free Date</Text>
          </View>
          <View style={paymentProgressStyle.genericPadding}>
                <Text style={commonStyle.largeNumberText}>{props.date}</Text>
          </View>
    </View>

    );
};

export default PaymentProgress;
