import React, {Component} from 'react';
import { SafeAreaView, StyleSheet, Text, View, Button, TouchableOpacity, Image, ScrollView, LogBox } from 'react-native';
import { Switch } from 'react-native-switch';
import { FontAwesome } from '@expo/vector-icons';
import CurrencyInput from 'react-native-currency-input';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import { DottedLine, commonStyle, ios, marginLeftBody, icons, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class NextPayment extends Component {

    constructor(props: Props) {
        super(props);
        this.state = {
            nextPayment: '573.28', //this.props.nextPayment
            nextPaymentDate: '11/23/2020', //this.props.nextPaymentDate
            bankAccountName: 'BofA Checking', //this.props.bankAccountName
            automaticPayments: true, //this.props.automaticPayments
            paymentFrequency: 'Monthly', //this.props.paymentFrequency
            monthlyPaymentAmount: 88.88 //this.props.monthlyPaymentAmount
        };
    }

    render() {
        const automaticPaymentsOnOff = this.state.automaticPayments === true ? 'ON':'OFF'

        return (
            <View style={{marginBottom: 30}}>
                <View style={{alignItems: 'center', marginTop: 20}}>
                    <Text style={commonStyle.blueLabel}>Next Payment</Text>
                    <Text style={commonStyle.largeNumberText}>${this.state.nextPayment}
                        <Text style={{fontSize: font13, fontWeight: 'normal', textTransform: 'lowercase'}}>  (monthly)</Text>
                    </Text>
                    <Text style={commonStyle.blueLabel}>Next Payment Due</Text>
                    <Text style={commonStyle.largeNumberText}>{this.state.nextPaymentDate}</Text>
                </View>
                <View style={{flexDirection: 'column', marginLeft: 20}}>
                    <Text style={commonStyle.blueLabel}>Monthly Payment Amount</Text>
                    <CurrencyInput style={{marginTop: 10, padding: 5, width: '80%', borderColor: '#4dd2f0', borderWidth: .3, borderRadius: 20, color: 'black'}} editable={false} unit="$" delimiter="," separator="." precision={2} value={this.state.monthlyPaymentAmount}/>
                    <Text style={commonStyle.blueLabel}>Payment Frequency</Text>
                    <Text style={{marginTop: 10, padding: 5, width: '80%', borderColor: '#4dd2f0', borderWidth: .3, borderRadius: 20}}>{this.state.paymentFrequency}</Text>
                    <Text style={commonStyle.blueLabel}>Monthly Payments Due</Text>
                    <Text style={{marginTop: 10, padding: 5, width: '80%', borderColor: '#4dd2f0', borderWidth: .3, borderRadius: 20}}>1st of Every Month</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop: 20, marginBottom: 40, marginLeft: 20}}>
                    <View style={{flexDirection: 'column'}}>
                      <Text style={commonStyle.blueLabel}>Bank Account</Text>
                      <Text style={{marginTop: 10, padding: 5, width: '120%', borderColor: '#4dd2f0', borderWidth: .3, borderRadius: 20}}>{this.state.bankAccountName}</Text>
                    </View>
                    <View style={{position: 'absolute', right: 40, flexDirection: 'column'}}>
                        <Text style={commonStyle.blueLabel}>Automatic{"\n"}Payments</Text>
                        <View style={styles.toggleButton}>
                            <Switch
                            value={this.state.automaticPayments}
                            disabled={false}
                            backgroundActive='white'
                            backgroundInactive='white'
                            circleActiveColor='#4dd2f0'
                            circleInActiveColor='#979fa1'
                            innerCircleStyle={{ width: 50, alignItems: 'center' }}
                            circleBorderWidth={0}
                            switchLeftPx={1}
                            switchRightPx={20}
                            renderInsideCircle={() =>
                                <Text style={{fontSize: font20, marginTop: 2, color: 'white'}}>
                                    {automaticPaymentsOnOff}
                                </Text>
                            }
                            renderActiveText={true}
                            renderInActiveText={true}/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    printIcon: {
        right: 60,
        position: 'absolute',
        marginTop: 10,
    },
    toggleButton: {
        marginTop: 10,
        backgroundColor: 'white',
        borderRadius: 30,
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 5,
        },
        shadowOpacity: .2,
        elevation: 20
    },
    value: {
        fontSize: 30,
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
});
