import React, {Component} from 'react';
import 'react-native-gesture-handler';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, Image, TouchableOpacity, Dimensions, Linking } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import WebView from 'react-native-webview';
import moment from 'moment';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class MonthlySummary extends Component {

    constructor(props: Props) {
        super(props);
        this.state = {
            activeYearsMonthsMap: {
                                  '2018':['October','November','December'],
                                  '2019':['January','February','March','April','May','June','July',
                                          'August','September','October','November','December'],
                                  '2020':['January','February','March','April','May','June','July',
                                          'August','September','October','November','December'],
                                  '2021':['January','February','March','April','May', 'June']
                                  },
            displayYear: '2021',
            showMonth: '',
            yearDropdownOpen: false
        }

        this.defaultState = JSON.parse(JSON.stringify(this.state));
        this.setDisplayYear = this.setDisplayYear.bind(this);

        this.props.navigation.addListener(
            'focus',
            payload => {
                if (this.props != null && this.props['route'] != null && this.props.route['params'] != null) {
                    if (this.props.route.params['openCurrentMonth'] === true) {
                        this.setState({
                            displayYear: String(moment().year()),
                            showMonth: moment().format('MMMM')
                        });
                    } else {
                        this.setState(this.defaultState);
                    }
                    this.props.navigation.setParams({'openCurrentMonth': null})
                }
            }
        );

    }

    ShowMonthPdf(month) {
        if (this.state.showMonth == month) {
            const pdfLink = "http://..."+month+this.state.displayYear+".pdf"
            const sampleLink = "http://www.smartcaptech.com/wp-content/uploads/sample.pdf"

            return (
                <View>
                    <TouchableOpacity onPress={() => Linking.openURL(sampleLink)}>
                        <Text style={styles.url}>{pdfLink}</Text>
                    </TouchableOpacity>
                </View>
            );
        }
    }

    setDisplayYear(callback) {
        this.setState(state => ({
            displayYear: callback(state.value)
        }));
    }

    render() {
        const activeYears = Object.keys(this.state.activeYearsMonthsMap)
        const activeYearsList = activeYears.map((year) => ({label:year, value:year})).sort().reverse();

        const activeMonthsList = this.state.activeYearsMonthsMap[this.state.displayYear].map((month, index) =>
            <View key={index}>
               <TouchableOpacity key={index} style={{alignSelf: 'flex-start'}} onPress={() => this.setState({ showMonth: month}) }>
                    <Text style={styles.monthLink}>
                    {month}
                    </Text>
                </TouchableOpacity>
                {this.ShowMonthPdf(month)}
            </View>

        );

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <HeaderComponent navigation={this.props.navigation}/>
                <ScrollView style={commonStyle.scrollViewBody}>
                    <View style={{flexDirection: 'row', marginTop: 20}}>
                        <Text style={commonStyle.title}>Monthly Summary</Text>
                        <Text style={{position: 'absolute', right: 95, marginTop: 15, fontSize: font15}}>Year</Text>
                        <View style={ios ? {zIndex: 10}:null, {position: 'absolute', right: 5}}>
                            <DropDownPicker
                            open={this.state.yearDropdownOpen}
                            items={activeYearsList}
                            value={this.state.displayYear}
                            style={{width: 90, borderWidth: 0}}
                            textStyle={{justifyContent: 'flex-start'}}
                            labelStyle={{justifyContent: 'flex-start',  marginLeft: 0}}
                            setValue={this.setDisplayYear}
                            onPress={open=>this.setState({ yearDropdownOpen:!this.state.yearDropdownOpen, showMonth:'' })}
                            onChangeValue={value=>this.setState({ yearDropdownOpen:!this.state.yearDropdownOpen, showMonth:'' })}
                            />
                        </View>
                    </View>
                    <View style={{alignSelf: 'flex-start', marginTop: 20, marginLeft: marginLeftBody}}>
                        <Text style={styles.yearLink}>{this.state.displayYear}</Text>
                        <View style={{marginLeft: 10}}>
                            {activeMonthsList}
                        </View>
                    </View>
                </ScrollView>
                <FooterComponent navigation={this.props.navigation}/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    monthLink: {
        marginBottom: 20,
        fontSize: font22,
        color: '#0b5cde',
        fontWeight: '600',
    },
    url: {
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: font18,
        fontFamily: 'medium',
        color: 'blue',
    },
    yearLink: {
        marginBottom: 10,
        fontSize: font22,
        fontWeight: '600',
    }

});