import * as React from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  View,
  Text,
  StatusBar,
  Divider,
  Image,
  Linking,
} from 'react-native';
import Dos from './Dos';
import Donts from './Donts';
import { commonStyle, DottedLine, moderateScale } from '../style/CommonStyle.js';

const WhatAffectsYourCreditScore = () => {
  return (
  <>
     <SafeAreaView style={[commonStyle.fullPageContainer, styles.background]}>
    <ScrollView contentInsetAdjustmentBehavior="automatic">

   <Text style={[commonStyle.sectionHeader, styles.sectionHeaderPadding]}>What Affects Your Credit Score?</Text>
   <DottedLine></DottedLine>
   <Text style={[commonStyle.sectionHeader, styles.sectionHeaderPadding]}>Pull Your Report or Dispute Errors</Text>
   <DottedLine></DottedLine>

    <Text style={styles.sectionDescription}>
      As you make progress on your Debt Management Plan, you'll want to periodically
      check your credit report for accuracy. If you find any errors, you can contact the
      three major credit reporting bureaus using the information below.{'\n'}
    </Text>
    <View style={styles.companyInfo}>
            <Image
              source={require('../assets/images/equifax.png')}
              resizeMode='contain'
              style={styles.imageStyle}
            />
            <Text onPress={()=>{Linking.openURL('tel:1-800-685-1111');}} style={styles.companyInfo}> 1-800-685-1111</Text>
            <Text style={styles.companyURL} onPress={() => Linking.openURL
            ('http://equifax.com')}>Equifax.com{'\n'} </Text>

            </View>
            <View style={styles.companyInfo}>
            <Image
              source={require('../assets/images/experian.png')}
              resizeMode='contain'
              style={styles.imageStyle}
            />
            <Text onPress={()=>{Linking.openURL('tel:1-888-397-3742');}} style={styles.companyInfo}> 1-888-397-3742</Text>
            <Text style={styles.companyURL} onPress={() => Linking.openURL
            ('http://experian.com')}>Experian.com{'\n'} </Text>
            </View>

            <View style={styles.companyInfo}>
            <Image
              source={require('../assets/images/transunion.png')}
              resizeMode='contain'
              style={styles.imageStyle}
            />
            <Text onPress={()=>{Linking.openURL('tel:1-800-916-8800');}} style={styles.companyInfo}> 1-800-916-8800</Text>
            <Text style={styles.companyURL} onPress={() => Linking.openURL
            ('http://transunion.com')}>Transunion.com </Text>
     </View>

          <Text style={[commonStyle.sectionHeader, styles.sectionHeaderPadding]}>Do's</Text>
          <DottedLine></DottedLine>
          <Text style={styles.sectionDescription}>
            <Dos />
          </Text>
          <Text style={[commonStyle.sectionHeader, styles.sectionHeaderPadding]}>Don'ts</Text>
          <DottedLine></DottedLine>
           <Text style={styles.sectionDescription}>
            <Donts />
            </Text>

      </ScrollView>
      </SafeAreaView>
</>

  );
};

const styles = StyleSheet.create({
  sectionDescription: {
    marginTop: 12,
    marginBottom: 12,
    fontSize: moderateScale(18),
    fontFamily: 'medium',
    color: 'black'
  },
  sectionHeaderPadding: {
    marginBottom: 12,
    marginTop: 12,
  },
    companyInfo: {
      fontSize: moderateScale(24),
      fontFamily: 'medium',
      alignItems: 'center',
      justifyContent: 'center',
      color: 'black'
    },
   companyURL: {
     fontSize: moderateScale(24),
     fontFamily: 'medium',
     color: 'blue',
     alignItems: 'center',
     justifyContent: 'center',
   },
    imageStyle: {
        width: 150,
        height: 50,
        flex: 1,
        resizeMode: 'contain'
    },
    background: {
        backgroundColor: 'white'
    }

});

export default WhatAffectsYourCreditScore;