import React, {Component} from 'react';
import { SafeAreaView, StyleSheet, Text, View, Button, TouchableOpacity, Image, ScrollView } from 'react-native';
import Moment from 'moment';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class MakeAPaymentStep2 extends Component {

    constructor(props: Props) {
        super(props);
        navigation = props.navigation;
        this.state = {
            paymentTypeSelected: props.route.params.paymentTypeSelected,
            paymentAmount: props.route.params.paymentAmountSelected,
            accountSelected: props.route.params.accountSelected,
            paymentDateSelected: props.route.params.paymentDateSelected,
            refreshStepOne: props.route.params.refreshStepOne
        };
    }

    static getDerivedStateFromProps(nextProps, state) {
        return {
            paymentTypeSelected: nextProps.route.params.paymentTypeSelected,
            paymentAmount: nextProps.route.params.paymentAmountSelected,
            accountSelected: nextProps.route.params.accountSelected,
            paymentDateSelected: nextProps.route.params.paymentDateSelected,
            refreshStepOne: nextProps.route.params.refreshStepOne
        }
    }


    render() {
        const monthly = this.state.paymentTypeSelected === 'Monthly'
        const noType = this.state.paymentTypeSelected === ''
        const today = Moment(new Date()).format('MM/DD/YYYY')

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <HeaderComponent navigation={this.props.navigation}/>
                <ScrollView>
                    <View style={[commonStyle.body, {marginLeft: 15}]}>
                        <Text style={styles.subTitle}>Step 2: Submit Your Payment</Text>
                        <Image source={require('../assets/images/step2Bar.png')} style={commonStyle.stepBar} />
                        <View style={{flexDirection: 'column', marginTop: 20, marginLeft: marginLeftBody}}>
                            <Text style={styles.label}>Payment Type</Text>
                            <Text style={styles.value}>{this.state.paymentTypeSelected} Payment</Text>
                            <Text style={styles.label}>Payment Amount</Text>
                            <Text style={styles.value}>${this.state.paymentAmount}</Text>
                            <Text style={styles.label}>Payment Date</Text>
                            <Text style={styles.value}>{this.state.paymentDateSelected}</Text>
                            <Text style={styles.label}>Bank Account</Text>
                            <Text style={styles.value}>#{this.state.accountSelected.substring(this.state.accountSelected.length - 4)}</Text>
                            { !monthly && !noType &&
                                <View>
                                    <Text style={styles.label}>Account</Text>
                                    <Text style={styles.value}>{this.state.accountSelected}</Text>
                                </View>
                            }
                        </View>
                        <View style={{margin: 30, flexDirection: 'row', alignSelf: 'flex-end'}}>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('PaymentPlan', {refreshMakeAPayment: false})}}>
                                <Text style={[styles.value, {alignSelf: 'flex-end', textDecorationLine: 'underline'}]}>Edit</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.props.route.params.refreshStepOne(true), this.props.navigation.navigate('PaymentPlan')}}>
                                <Text style={[styles.value, {marginLeft: 20, alignSelf: 'flex-end', textDecorationLine: 'underline'}]}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity title="Submit Payment" style={commonStyle.blueButtonTouchable}
                            onPress={() => { this.props.route.params.refreshStepOne(true),
                                             this.props.navigation.navigate('MakeAPayment-StepThree', {
                                             paymentAmount: this.state.paymentAmount,
                                             paymentDate: this.state.paymentDateSelected,
                                             accountSelected: this.state.accountSelected
                                            })}
                            }>
                            <Text style={commonStyle.blueButtonText}>Submit Payment</Text>
                        </TouchableOpacity>
                        <View style={{margin: 5, marginTop: 30}}>
                            <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                                <Image source={require('../assets/images/questionTriangle.png')} style={{height: 20, width: 20}} />
                                <Text style={[styles.label, {marginTop: 0, marginLeft: 10, fontSize: font18, textTransform: 'uppercase'}]}>IMPORTANT INFORMATION</Text>
                            </View>
                            <Text style={[styles.value, {marginTop: 10, fontSize: font13}]}>
                                Today {today}, you are about to authorize a single ACH or electronic debit via the internet. The payment will be made to Take Charge America from your bank account ending in #{this.state.accountSelected.substring(this.state.accountSelected.length - 4)} on {this.state.paymentDateSelected} in the amount of {this.state.paymentAmount} for your Debt Management Plan payment.{'\n'}{'\n'}
                                Your payment should be applied to your Account on the payment date you selected; however, it may take up to 3 business days to complete the funds transfer. We will disburse payment to your creditors as soon as the funds clear.{'\n'}{'\n'}
                                You may cancel this payment online 2 days prior to the payment date. Cancellations after this time will need to be requested over the phone by contacting our Customer Service department at 800-823-7396, Monday-Friday 6am-6pm MST.{'\n'}{'\n'}
                                To continue receiving benefits on your DMP, creditors require payment every thirty days. If you miss a payment or make a partial payment, please understand that creditors reserve the right to drop you from the program.
                                Each creditor has their own policy regarding missed payments and it’s best to speak to a counselor if you’re having trouble making regular, on-time payments. We will work with you to ensure you continue receiving the full benefits of your DMP so you can successfully pay off your debt.{'\n'}{'\n'}
                            </Text>
                        </View>
                    </View>
                    <View style={{height: 80}}/>
                </ScrollView>
                <FooterComponent navigation={this.props.navigation}/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    label: {
        marginTop: 30,
        fontSize: font15,
        fontWeight: 'bold',
        color: darkBlue,
        textTransform: 'uppercase',
    },
    pickerContainer: {
        zIndex: ios ?  5:null,
        marginLeft: -10,
        borderWidth: 0,
        borderColor: 'white',
        borderRadius: 0,
        shadowColor: 'white',
    },
    subTitle: {
        marginTop: 20,
        fontSize: font20,
        color: '#4dd2f0',
        fontWeight: 'bold',
    },
    value: {
        marginTop: 10,
        fontSize: font15,
    },
});

var updatedStates = false

