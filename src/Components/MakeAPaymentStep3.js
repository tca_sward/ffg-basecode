import React, {Component} from 'react';
import { SafeAreaView, StyleSheet, Text, View, Button, TouchableOpacity, Image, ScrollView } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import moment from 'moment';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class MakeAPaymentStep3 extends Component {

    constructor(props: Props) {
        super(props);
        navigation = props.navigation;
        this.state = {
            confirmationNumber: 'EFT01279', //this.props.confirmationNumber
            paymentAmount: props.route.params.paymentAmount,
            accountSelected: props.route.params.accountSelected,
            paymentDate: props.route.params.paymentDate,
        };

    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <HeaderComponent navigation={this.props.navigation}/>
                <ScrollView>
                    <View style={[commonStyle.body, {marginLeft: 15}]}>
                        <View>
                            <Text style={styles.subTitle}>Step 3: Payment Confirmation</Text>
                            <Image source={require('../assets/images/step3Bar.png')} style={commonStyle.stepBar} />
                            <View style={{margin: 20}}>
                                <Text style={styles.label}>Confirmation Number: {this.state.confirmationNumber}</Text>
                                <Text style={styles.value}>
                                    You have <Text style={{fontStyle: 'italic'}}>successfully</Text> scheduled your payment. An email confirmation will be sent to the primary email address on file.{'\n'}{'\n'}
                                    The payment will be made to Take Charge America from your bank account ending in #{this.state.accountSelected.substring(this.state.accountSelected.length - 4)} on {this.state.paymentDate} in the amount of ${this.state.paymentAmount} for your Debt Management Plan payment.{'\n'}{'\n'}
                                    Your payment should be applied to your Account on the payment date you selected; however, it may take up to 3 business days to complete the funds transfer. We will disburse payment to your creditors as soon as the funds clear.{'\n'}{'\n'}
                                    To continue receiving benefits on your DMP, creditors require payment every thirty days. If you miss a payment or make a partial payment, please understand that creditors reserve the right to drop you from the program.
                                    Each creditor has their own policy regarding missed payments and it’s best to speak to a counselor if you’re having trouble making regular, on-time payments. We will work with you to ensure you continue receiving the full benefits
                                    of your DMP so you can successfully pay off your debt.
                                </Text>
                            </View>
                            <TouchableOpacity title="Done" style={[commonStyle.blueButtonTouchable, {marginTop: 20}]} onPress={() => { this.props.navigation.navigate('PaymentPlan', {ViewScheduledPaymentsCollapsed: false, refresh: true}) }}>
                                <Text style={commonStyle.blueButtonText}>Done</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                <View style={{height: 120}}/>
                </ScrollView>
                <FooterComponent navigation={this.props.navigation}/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    label: {
        marginTop: 15,
        fontSize: 13,
        fontWeight: 'bold',
        color: darkBlue,
        textTransform: 'uppercase',
    },
    subTitle: {
        fontSize: font20,
        color: '#4dd2f0',
        fontWeight: 'bold',
    },
    value: {
        marginTop: 10,
        fontSize: 13,
    },
});


