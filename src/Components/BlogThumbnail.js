import React from 'react';
import { Text, View, Image, TouchableOpacity, Linking} from 'react-native';
import blogThumbnailStyle from '../style/BlogThumbnailStyle';
import { commonStyle, moderateScale, verticalScale } from '../style/CommonStyle.js';

const BlogThumbnail = (props) => {
  return (
    <View style={{height: verticalScale(200), marginBottom: 30, marginTop: 30}}>
        <View style={commonStyle.roundedRectangleTopStyle}>
            <Image style={blogThumbnailStyle.imageStyle} source={{uri: props.image}} />
        </View>
        <View style={commonStyle.roundedRectangleBottomStyle}>
            <Text style={commonStyle.paragraphText}>
                {props.title}
            </Text>
            <TouchableOpacity onPress={() => Linking.openURL(props.link)}>
                <Text style={commonStyle.linkText}>
                    Read More on our Blog >>
                </Text>
            </TouchableOpacity>
        </View>
    </View>
  );
};

export default BlogThumbnail;
