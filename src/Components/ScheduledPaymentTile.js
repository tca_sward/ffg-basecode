import React, { useState } from 'react';
import { Text, View, Dimensions, PixelRatio, StyleSheet, Alert, Modal, Pressable } from 'react-native';
import currentPaymentStyle from '../style/CurrentPaymentStyle';

const autopay = 'Monthly Payment - Autopay';
const monthlyPayment = 'Monthly Payment';
/*
 Properties:
    paymentDate - (string) date of payment
    paymentAmount - (string) amount paid
    isAutoPay - (boolean) true if auto payment
    canCancel - (boolean) true if cancellation is possible

 Sample Usage:
      <ScheduledPaymentTile
            paymentDate='12/24/2020'
            paymentAmount='100.80'
            isAutoPay={false}
            canCancel={true}> </ScheduledPaymentTile>

 TODO: The message popup for an unqualified client is included in the code, but is not rendered for now
 */

const ScheduledPaymentTile = (props) => {
    const CancelPopUp = () => {
      const [modalVisible, setModalVisible] = useState(false);
      return (
        <View style={currentPaymentStyle.centeredView}>
          <Modal
            animationType="none"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible(!modalVisible);
            }}
          >
            <View style={currentPaymentStyle.modalView}>
                        <Pressable
                            style={[currentPaymentStyle.xButtonStyle]}
                            onPress={() => setModalVisible(!modalVisible)}
                          >
                            <Text style={currentPaymentStyle.xButtonText}>X</Text>
                          </Pressable>
            <View style={currentPaymentStyle.centeredView}>

                <Text style={currentPaymentStyle.pageHeader}>Payment Cancellation</Text>
                <Text style={currentPaymentStyle.fieldLabel}>PAYMENT DATE: {props.paymentDate} {'\n'}
                    PAYMENT AMOUNT: ${props.paymentAmount}</Text>
                <Text style={currentPaymentStyle.descriptionText}>If you miss or make a partial payment,
                please understand that creditors reserve the right to drop you from the program. Each
                creditor has their own policy regarding missed payments and it's best to speak to a counselor
                if you're having trouble making regular, on-time payments. We will work with you to ensure
                you continue receiving the full benefits of your DMP so you can successfully pay off your debt.</Text>
                <Text style={currentPaymentStyle.fieldLabel}>Are you sure you want to cancel this payment?</Text>

              </View>
                  <View style={currentPaymentStyle.buttonView}>
                      <Pressable
                        style={[currentPaymentStyle.buttonStyle]}
                        onPress={() => setModalVisible(!modalVisible)}
                      >
                        <Text style={currentPaymentStyle.buttonText}>NO</Text>
                      </Pressable>

                      <Pressable
                        style={[currentPaymentStyle.buttonStyle]}
                        onPress={() => setModalVisible(!modalVisible)}
                      >
                        <Text style={currentPaymentStyle.buttonText}>YES</Text>
                      </Pressable>
                  </View>

            </View>
          </Modal>

          <Pressable
            style={[currentPaymentStyle.cancelButton]}
            onPress={() => setModalVisible(true)}
          >
            <Text style={currentPaymentStyle.cancel}>CANCEL</Text>
          </Pressable>
        </View>
      );
    };
  return (
    <View style={currentPaymentStyle.container}>
        <View style={currentPaymentStyle.leftBase}>
            {props.isAutoPay ? <Text style={currentPaymentStyle.categoryText}>{autopay}</Text>
                : <Text style={currentPaymentStyle.categoryText}>{monthlyPayment}</Text>}
            <Text style={currentPaymentStyle.titleText}>{props.title}</Text>
        </View>
        <View style={[currentPaymentStyle.rightBase, {borderColor: 0}]}>
            <Text style={currentPaymentStyle.fieldLabel}>PAYMENT DATE</Text>
            <Text style={currentPaymentStyle.descriptionText}>
                {props.paymentDate}
            </Text>
            <Text style={currentPaymentStyle.fieldLabel}>PAYMENT AMOUNT</Text>
                <Text style={currentPaymentStyle.descriptionText}>
                ${props.paymentAmount}
            </Text>
        </View>
        <View style={[currentPaymentStyle.cancelBase, {borderColor: 0}]}>
            {!props.isAutoPay && props.canCancel && <CancelPopUp></CancelPopUp>}
            {!props.isAutoPay && !props.canCancel &&  <Text style={currentPaymentStyle.inProgress}>IN PROGRESS</Text>}


        </View>
    </View>
  );
};

export default ScheduledPaymentTile;