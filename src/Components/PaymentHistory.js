import React, {Component} from 'react';
import 'react-native-gesture-handler';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, Image, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import CurrencyInput from 'react-native-currency-input';
import { FontAwesome } from '@expo/vector-icons';
import Moment from 'moment';
import _ from 'lodash';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import { commonStyle, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue } from  '../style/CommonStyle.js';


export default class PaymentHistory extends Component {

    constructor(props: Props) {
        super(props);

        this.allPayments = [
            {Date: '2021-04-10', Account: 'BLAIR - ****8644', Amount: 19.00},
            {Date: '2021-03-04', Account: 'BLAIR - ****8644', Amount: 23.00},
            {Date: '2020-12-08', Account: 'BLAIR - ****8644', Amount: 5.00},
            {Date: '2020-11-08', Account: 'BLAIR - ****8644', Amount: 5.00},
            {Date: '2020-06-03', Account: 'BLAIR - ****8644', Amount: 5.00},
            {Date: '2019-09-12', Account: 'BLAIR - ****8644', Amount: 100.00},
            {Date: '2019-04-10', Account: 'BLAIR - ****8644', Amount: 80.00}
        ] //this.props.allPayments


        this.state = {
            filteredPayments: this.allPayments.filter(item => {
                              return Moment(item.Date) > Moment(new Date()).subtract(14, 'months')
                            }),
            yearsActive: [2021, 2020, 2019],
            filterByPeriod: 'Recent',
            filterDropdownOpen: false
        };

        this.setFilterBy = this.setFilterBy.bind(this);

    }

    setFilterBy = (callback) => {
        this.setState(state => ({
            filterByPeriod: callback(state.value),
            filteredPayments: this.allPayments.filter(item => {
                             if (callback(state.value) === 'Recent') {
                                  return Moment(item.Date) > Moment(new Date()).subtract(14, 'months');
                             } else {
                                  return Moment(item.Date).year() === parseInt(callback(state.value));
                             }
                     })

        }));
    }


    render() {

        const sortTable = (column) => {
            const newDirection = direction === 'desc' ? 'asc' : 'desc'
            const sortedData = _.orderBy(this.state.filteredPayments, [column], [newDirection])
            selectedColumn = column
            direction = newDirection
            this.state.filteredPayments = sortedData
        }

        const tableHeader = () => (
            <View style={styles.tableHeader}>
                {
                columns.map((column, index) => {
                    {
                    return (
                        <TouchableOpacity key={index} style={styles.columnHeader} onPress={()=> sortTable(column)}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={styles.columnHeaderTxt}>
                                    {column + ' '}
                                    { column !== 'Account' && selectedColumn === column &&
                                     <FontAwesome name={direction === 'desc' ? 'caret-down':'carat-up'} style={{fontSize: 18, color: '#79807c'}}/>
                                    }
                                </Text>
                            </View>
                        </TouchableOpacity>
                    )
                    }
                })
                }
            </View>
        )

        var filterByList = this.state.yearsActive.map((year) => ({label:year.toString(), value:year.toString()}));
        filterByList.unshift({label:'Recent', value:'Recent'})


        return (
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <HeaderComponent navigation={this.props.navigation}/>
                <ScrollView style={{marginLeft: 20}}>
                    <View style={{zIndex: 10, marginLeft: 10}}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={commonStyle.title}>Payment History</Text>
                            <TouchableOpacity style={{position: 'absolute', right: 40}} onPress={() => this.props.navigation.navigate('PaymentPlan', {})}>
                                <Text style={[styles.value, {textDecorationLine: 'underline'}]}>
                                    Back
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={ios? {zIndex: 10}:null, {marginTop: 20, marginLeft: 30}}>
                            <Text style={styles.label}>Filter By</Text>
                            <View style={styles.pickerContainer}>
                                <DropDownPicker
                                    open={this.state.filterDropdownOpen}
                                    style={{width: '80%', borderWidth: 0}}
                                    items={filterByList}
                                    value={this.state.filterByPeriod}
                                    textStyle={{justifyContent: 'flex-start'}}
                                    labelStyle={{justifyContent: 'flex-start', marginLeft: 0}}
                                    setValue={this.setFilterBy}
                                    onPress={open=>this.setState({ filterDropdownOpen:!this.state.filterDropdownOpen })}
                                    onChangeValue={value=>this.setState({ filterDropdownOpen:!this.state.filterDropdownOpen })}
                                    />
                            </View>
                        </View>
                    </View>
                    <View style={styles.container}>
                        <FlatList
                        data={this.state.filteredPayments}
                        style={{width:'90%'}}
                        keyExtractor={(item, index) => index+''}
                        ListHeaderComponent={tableHeader}
                        stickyHeaderIndices={[0]}
                        renderItem={({item, index})=> {
                        return (
                            <View style={[styles.tableRow, {backgroundColor: index % 2 == 1 ? '#F0FBFC' : 'white'}]}>
                              <Text style={[styles.columnRowTxt]}>{Moment(item.Date).format('MM/DD/YYYY')}</Text>
                              <Text style={styles.columnRowTxt}>{item.Account}</Text>
                              <CurrencyInput style={[styles.columnRowTxt, {color: 'black'}]} editable={false} unit="$" delimiter="," separator="." precision={2} value={item.Amount}/>
                            </View>
                        )
                        }}
                          />
                    </View>
                    <Text style={{marginTop: 10, alignSelf: 'center'}}>Showing 1 to {this.state.filteredPayments.length} of {this.state.filteredPayments.length} entries</Text>
                </ScrollView>
                <View>
                    <FooterComponent navigation={this.props.navigation}/>
                </View>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    label: {
        marginTop: 15,
        fontSize: font15,
        fontWeight: 'bold',
        color: darkBlue,
        textTransform: 'uppercase',
    },
    columnHeader: {
        width: '30%',
        justifyContent: 'center',
        alignItems:'center'
    },
    columnHeaderTxt: {
        fontWeight: 'bold',
    },
    columnRowTxt: {
        width:'32%',
        flex: 1,
        textAlign:'center',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#abaaa7',
    },
    pickerContainer: {
        minHeight: 30,
        width: 120,
        marginLeft: -10,
    },
    tableHeader: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#abaaa7'
    },
    tableRow: {
        height: 40,
        flexDirection: 'row',
        alignItems:'center',
    },
    value: {
        fontSize: font15,
        color: 'blue'
    },
});


const columns = [
        'Date',
        'Account',
        'Amount',
      ]

var selectedColumn = null
var direction = 'asc'



