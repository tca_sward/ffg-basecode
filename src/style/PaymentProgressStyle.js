import {StyleSheet} from 'react-native'

import {moderateScale, itemColor} from './CommonStyle.js'

const paymentProgressStyle = StyleSheet.create({
    genericPadding: {
        marginTop: 5
    },
    progressBarStyle: {
        backgroundColor: '#dceef2',
        height: moderateScale(40),
        borderRadius: 30,
        width: '90%',
        alignSelf: 'center'
    },
    aboveProgressBarStyle: {
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 5,
        width: '90%',
        shadowOpacity: .50,
        marginBottom: '10%',
        display: 'flex',
        marginLeft: '5%'
    },
    linkText: {
        fontSize: moderateScale(10), 
        margin: '5%',
        marginTop: '15%',
        textAlign: 'center', 
        color: itemColor
    },
});

export default paymentProgressStyle;
