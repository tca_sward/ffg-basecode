import {StyleSheet} from 'react-native';
import { useFonts } from 'expo-font';
import {commonStyle, lightBlueText, moderateScale, itemColor, verticalScale} from './CommonStyle.js';

const numTitleLines = 3;
const numDescriptionLines = 18;

const accordionStyle = StyleSheet.create({
    sectionHeader:{
        fontFamily: 'medium',
        fontSize: moderateScale(22, 0.5),
        color: '#1b2126',
        width: '60%',
        textAlign: "left",
        marginTop: 12,
    },
    sectionTitle: {
        fontFamily: 'medium',
        margin: 0,
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'left',
        color: 'black',
    },
    sectionDescription: {
        fontFamily: 'medium',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 16,
        fontWeight: 'normal',
        textAlign: 'left',
        color: 'black'
    },
    pageTitle: {
        fontSize: moderateScale(48, 0.5),
        marginBottom: 12,
        color: lightBlueText,
        fontWeight: 'bold',
        fontFamily: 'medium',
    },
    contactInfo: {
        fontSize: moderateScale(20, 0.5),
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center',
    },
    hotlineTitle: {
         fontSize: moderateScale(20, 0.5),
         fontWeight: 'bold',
         color: 'black',
         textAlign: 'left',
         marginTop: 20,
    },
    link: {
        fontSize: moderateScale(20, 0.5),
        color: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default accordionStyle;