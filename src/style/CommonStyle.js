// This document serves as a reference for other style sheets with common style uses
import { useFonts } from 'expo-font';
import React from 'react';
import { Platform, Dimensions, StyleSheet, View, Text, Image } from 'react-native';
import DashedLine from 'react-native-dashed-line';

const DottedLine = () => {
    return (<DashedLine dashLength={2}
                        dashThickness={5}
                        dashGap={5}
                        dashColor='#bed1d6'
                        dashStyle={{ borderRadius: 5 }}
           />)};

const HeaderLogoImage = () => (
    <Image
        style={{
            flex: 1,
            alignSelf: 'stretch',
            marginTop: '3%',
            width: moderateScale(10),
            height: verticalScale(50),
            }}
        source={require('../assets/images/take-charge-america-logo.png')}
    />);

//start FONT SCALE https://medium.com/soluto-engineering/size-matters-5aeeb462900a
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
const lightBlueText = '#417fd2'
const darkBlueText = '#225184'
const itemColor = '#417fd2'

const ios = Platform.OS === 'ios';
const marginLeftBody = 20
const darkBlue = 'rgba(1,74,127,255)'
const font13 = 13
const font15 = 15
const font18 = 18
const font20 = 20
const font22 = 22

const commonStyle = StyleSheet.create({
    // Text specific styles

    // Biggest size font; labeling full page
    pageHeader : {
        fontSize: moderateScale(26, 0.5),
        marginBottom: 12,
        color: '#417fd2',
        fontWeight: 'bold',
    },
    // Next biggest font; font size for sections on page
    sectionHeader:{
        fontFamily: 'medium',
        fontSize: moderateScale(25, 0.5),
        color: '#1b2126',
        width: '100%',
        textAlign: "left"
    },
    // Next biggest font; font size for sections on page
    sectionLeft:{
        fontFamily: 'medium',
        fontSize: moderateScale(22, 0.5),
        color: '#1b2126',
        width: '60%',
        textAlign: "left"
    },
    // Font format for text accompanying section header (on right side)
    sectionRight:{
        textAlign: "right",
        fontFamily: 'bold',
        fontSize: moderateScale(13),
        textTransform:"uppercase",
        fontWeight: "bold",
        color: '#225184',
        alignItems: "center",
        width:'40%',
    },
    // Font format for field labels
    fieldLabel:{
        fontFamily: 'medium',
        fontSize: moderateScale(13, 0.5),
        textTransform:"uppercase",
        fontWeight: "bold",
        color: '#225184',
        paddingLeft: 20,
        marginBottom: moderateScale(5, 0.5),
    },
    // Font formats for navigation text (ie. my account or logout)
    navigationItem:{
        fontFamily:'medium',
        textTransform: 'uppercase',
        margin: 20,
        textAlign: 'right',
    },
    // Text for footer
    footerText: {
        fontSize: moderateScale(13, 0.5),
        color:'#ffffff',
        fontFamily: 'medium',
        letterSpacing: 0,
    },
    // Format for most commonly used text size
    paragraphText: {
        fontFamily: 'medium',
        margin: 0,
        fontSize: 16,
        fontWeight: 'normal',
        textAlign: 'left',
        marginBottom: 12,
    },
    // Format for slightly smaller text
    finePrint:{
        fontSize: moderateScale(13, 0.5),
        fontStyle: 'italic',
        marginBottom: 12 ,
    },
    // Format for any text inputs
    textInput:{
        fontFamily: 'medium',
        borderRadius: 25,
        height: 50,
        paddingLeft: 15,
        fontSize: 15,
        borderColor: '#43b7d7',
        borderWidth: 1,
        marginBottom:12,
    },
    largeNumberText: {
        fontSize: moderateScale(38),
        textAlign: 'center',
        fontFamily: 'bold',
        paddingBottom: '8%'
    },
    linkText: {
        fontSize: moderateScale(15), 
        margin: "5%", 
        textAlign: 'center', 
        color: lightBlueText
    },

    // Specific Item Styles 
    footerStyle: {
        padding:15,
        backgroundColor:'#1b2126',
    },
    buttonStyle: {
        marginTop:10,
        paddingTop:15,
        paddingBottom:15,
        marginLeft:30,
        marginRight:30,
        backgroundColor:'#417fd2',
        borderRadius:25,
    },
    LeftAndRight:{
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 12,
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    fontAwesomeStyle:{
        fontSize: moderateScale(15),
        color:'#43b7d7',
    },
    roundedRectangleBottomStyle: {
        backgroundColor: 'white',
        borderBottomRightRadius: 20, 
        borderBottomLeftRadius: 20,
        alignItems: "center",
        paddingTop: moderateScale(10),
        marginBottom: '5%',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.2,
        shadowRadius: 2,  
        elevation: 3
    },
    roundedRectangleTopStyle: {
        width: "100%",
        height: "70%", 
        backgroundColor: 'white',
        borderTopRightRadius: 20, 
        borderTopLeftRadius: 20,
    },

    // Full page styles
    fullPageContainer: {
        flexDirection: 'column',
        height: '100%',
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: moderateScale(20, 0.75),
        margin: 20,
        marginBottom: 40,
        fontFamily: 'medium',
    },

    // Other item styles
    blueButtonTouchable: {
        width: '70%',
        padding: 10,
        marginTop: 5,
        marginLeft: 10,
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: '#1266b5',
        borderRadius: 30,
        borderWidth: 0,
    },
    blueButtonText: {
        fontSize: 18,
        textTransform: 'uppercase',
        color: 'white',
        fontWeight: 'bold',
    },
    blueLabel: {
        marginTop: 10,
        fontFamily: 'condensed',
        fontSize: font18,
        fontWeight: 'bold',
        color: darkBlue,
        textTransform: 'uppercase',
    },
    body: {
        marginTop: 20,
        marginRight: 20,
    },
    icons: {
        margin: 5,
        fontSize: 20,
        color:'#43b7d7',
    },
    navIcons: {
        margin: 0,
        padding: 0,
        alignSelf: 'center',
        position: 'absolute',
        right: 5
    },
    navigationItems: {
        textAlign: 'right',
        fontFamily: 'condensed',
        fontSize: font20,
        textTransform: 'uppercase',
        color: 'black',
    },
    scrollViewBody: {
        marginLeft: 20,
        marginBottom: 90,
    },
    stepBar: {
        height: 20,
        width: 150,
        marginTop: 20,
        alignSelf: 'center',
    },
    title: {
        fontSize: 25,
    },
    titleBold: {
        fontSize: 22,
        fontWeight: 'bold',
    },

    // Other
    dottedLine: {
        borderWidth: moderateScale(3, 0.5),
        borderStyle: 'dotted',
        borderColor: 'black',
        marginBottom: 12,
        marginBottom: '5%'
    },

    // login stuff
    containerBlack: {
        backgroundColor: 'black',
        height: '15%',
        width: '100%',
        color: "white",
    },
    loginButton: {
        color: "rgba(1,74,127,255)"
    },
    loginButtonSection: {
        width: '40%',
        alignSelf: 'center',
        backgroundColor:'rgba(1,74,127,255)',
        borderRadius: 8,
        marginTop: '5%',
        marginBottom: '10%'
    },
    input: {
        margin: 1,
        height: 40,
        width: 300,
        borderColor: 'rgba(1,74,127,255)',
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center'
    },
    standardText: {
        color: 'rgba(1,74,127,255)'
    },
    centerText: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'rgba(1,74,127,255)',
    },
    underlineCenterText: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'rgba(1,74,127,255)',
        textDecorationLine: 'underline'
    },
    centerTextSmall: {
        fontSize: 10,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white'
    },
    centerTextWhite: {
        fontSize: 25,
        fontWeight: "bold",
        textAlign: 'center',
        color: 'white'
    },
    centerTextBold: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: "bold",
        color: 'rgba(1,74,127,255)',
        fontSize: 14
    },
    redText: {
        color: "red"
    },
    smallText: {
        fontSize: 10,
        color: 'rgba(1,74,127,255)'
    }
})

export {DottedLine, HeaderLogoImage, commonStyle, lightBlueText, darkBlueText, moderateScale, verticalScale,
itemColor, ios, marginLeftBody, font13, font15, font18, font20, font22, darkBlue}