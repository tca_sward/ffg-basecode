import {StyleSheet} from 'react-native'
import { moderateScale } from '../style/CommonStyle.js'

const blogThumbnailStyle = StyleSheet.create({
    imageStyle: {
        width: "100%",
        height: "100%", 
        borderTopRightRadius: 20, 
        borderTopLeftRadius: 20,
    },
    bottomStyle: {
        backgroundColor: 'white',
        borderBottomRightRadius: 20, 
        borderBottomLeftRadius: 20,
    },
    descriptionText: {
        fontSize: moderateScale(15), 
        margin: "3%", 
        textAlign: 'center'
    }
});

export default blogThumbnailStyle;