import {Platform, StyleSheet, Dimensions, PixelRatio} from 'react-native';
import { moderateScale } from './CommonStyle.js';
import { useFonts } from 'expo-font';

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  } = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
    const newSize = size * scale
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(newSize))
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    }
};

const currentPaymentStyle = StyleSheet.create({
    container: {
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '10%',
        marginBottom: '10%',
        borderRadius: 20,
        flexDirection: 'row',
        flex: 1,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: .50,
    },
    pageHeader : {
        fontSize: moderateScale(20, 0.5),
        marginBottom: 10,
        color: '#225184',
        fontWeight: 'bold'
    },
    leftBase: {
        borderBottomLeftRadius: 20,
        borderTopLeftRadius: 20,
        backgroundColor: '#238bdb',
        width: '30%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    rightBase: {
        backgroundColor: 'white',
        width: '45%',
        alignItems: 'flex-start',
        textAlign: 'left',
        margin: 0
    },
    cancelBase: {
        backgroundColor: 'white',
        borderBottomRightRadius: 20,
        borderTopRightRadius: 20,
        width: '25%',
        justifyContent: 'center',
        flexDirection: 'row',
        marginLeft: -10
    },
    categoryText: {
        textAlign: 'center',
        fontSize: normalize(14),
        color: 'white',
        marginTop: '30%',
    },
    titleText: {
        textAlign: 'center',
        marginTop: '20%',
        fontSize: normalize(15),
        color: 'white'
    },
    descriptionText: {
        fontSize: normalize(12),
        margin: "3%",
        marginBottom: 0,
        textAlign: 'left',
    },
    fieldLabel:{
        fontFamily: 'medium',
        fontSize: moderateScale(13, 0.5),
        fontWeight: "bold",
        color: '#225184',
        margin: '3%',
        textAlign: 'center',
    },
    cancel: {
        fontSize: normalize(12),
        color: '#225184',
        margin: '5%',
        textAlign: 'center',
        textDecorationLine: 'underline',
        marginTop: '50%'
    },
   inProgress: {
       fontSize: moderateScale(11),
       color: '#225184',
       margin: '5%',
       textAlign: 'center',
       marginTop: '50%',
       flexShrink: 1,
   },
   buttonStyle: {
       padding: 15,
       marginTop: 15,
       marginBottom: 15,
       marginLeft: 10,
       marginRight: 10,
       backgroundColor:'#417fd2',
       borderRadius:25,
       width: "45%"
   },
    xButtonStyle: {
        margin: 0,
        backgroundColor:'white',
    },
    xButtonText: {
      fontFamily: 'bold',
      fontSize: moderateScale(20, 0.5),
      fontWeight: "bold",
      color: 'black',
      textAlign: 'right',
    },
    buttonText: {
        fontFamily: 'bold',
        fontSize: moderateScale(20, 0.5),
        textTransform:"uppercase",
        fontWeight: "bold",
        color: 'white',
        textAlign: 'center',
    },
    buttonView: {
        flexDirection: "row",
        justifyContent: 'space-around',
    },
    cancelButton: {
        alignItems: "center",
    },
    centeredView: {
        alignItems: "center",
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        padding: 15,
        paddingTop: 15,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    paymentView: {
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: 'center',
}
});

export default currentPaymentStyle;