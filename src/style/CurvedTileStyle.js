import {Platform, StyleSheet, Dimensions, PixelRatio} from 'react-native'

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  } = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
    const newSize = size * scale 
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(newSize))
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    }
};

const curvedTileStyle = StyleSheet.create({
    container: {
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '10%',
        marginBottom: '10%',
        borderRadius: 20,
        flexDirection: 'row',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: .50,
        elevation: 20,
        height: SCREEN_HEIGHT / 3.5
    },
    leftBase: {
        borderBottomLeftRadius: 20, 
        borderTopLeftRadius: 20,
        backgroundColor: '#238bdb',
        width: '40%'
    },
    leftCircle: {
        borderRadius: SCREEN_HEIGHT / 4.5, 
        backgroundColor: '#6bb5ed',
        width: SCREEN_HEIGHT / 9,
        height: SCREEN_HEIGHT / 9,
        marginLeft: '20%',
        marginTop: '20%'
    },
    rightBase: {
        backgroundColor: 'white',
        borderBottomRightRadius: 20, 
        borderTopRightRadius: 20,
        width: '60%'
    },
    percentText: {
        textAlign: 'center', 
        marginTop: '30%', 
        fontSize: normalize(20), 
        color: 'white'
    },
    titleText: {
        textAlign: 'center', 
        marginTop: '20%', 
        fontSize: normalize(15), 
        color: 'white'
    },
    descriptionText: {
        fontSize: normalize(12), 
        margin: "5%", 
        textAlign: 'center', 
        marginTop: '30%'
    },
    impactText: {
        fontSize: normalize(10), 
        margin: "5%", 
        textAlign: 'right', 
        color: 'red', 
        fontWeight: 'bold'
    }
});

export default curvedTileStyle;