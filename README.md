# README #

##Take Charge America Mobile App

### Setting up the Application and Installing Packages###

From command line,
1. "cd into src"
2. "npm install"

Please install the following libraries:

3. "npm install react-native-dashed-line"
4. "npm install react-native-paper"
The expandable web component (based on List and ListAccordian) overwrites some of the files from the
default react-native-paper library. Once react-native paper is installed, copy the
"react-native-paper" folder in "overwrite" over to node_modules.

5. "npm install react-native-elements"
6. "npm install react-native-vector-icons"
7. "npm install --save-dev typescript @types/jest @types/react @types/react-native @types/react-test-renderer"

8. "react-native link"


### Running the application - Android and iOS simulators

9. "npm start"

